# Contributing

* If you want to report an issue, please refer to the [Issues](#issues) section.
* If you want to contribute to the project, please refer to the [Development](#development) section.

## Issues

If you want to report about a problem or request a feature — create an issue.

### Step-by-step guide to creating issues

1. Write a succinct issue title that reflects the essence of the issue.
2. Provide details of the issue in the description.
3. Set the appropriate labels:
    * If this is a problem, set the *fix* label.
    * If this is a feature request, set the *feature* label.
    * If this is issue relates to the documentation, set the *documentation* label.
    * If this is issue relates to the environment e.g. CI/CD, server configs etc., set the *environment* label.
    * If this is issue relates to the main application, set the *project* label.
4. Send an issue.

### Issues management

* If the issue is not accepted, the reason is described and the issue is closed.
* If the issue is accepted, a milestone and assignee are assigned.

## Development

Contributes to this project must follow approved gitflow.

### Branches

#### The 'master' branch

* The **master** branch contains the latest production version.
* Source branch for the **master** branch is only the **develop** branch.
* All merge requests to the **master** branch should have title in the semantic format: MAJOR.MINOR.PATCH e.g.: `1.3.2`,
  squash all changes and close milestone with the same title.

#### The 'develop' branch

* The **develop** branch contains the latest development version.
* The **develop** branch is the source and target branch for any dev branches.

#### The dev branches

* The dev branches creates from the project issues.
* The dev branches should be prefixed `feature/` if the issue is labeled as *feature*, or `fix/` if the issue is
  labeled as *fix*.
* The dev branches should start from the **develop** branch.
* The dev branches should end in the **develop** branch with merge request.
* Any dev branches name should follow kebab-case.
* Merge request of the dev branch to **develop** branch should delete this dev branch after accept.

### Tags

* Previous versions are available through tags.
* Versions have a semantic format: MAJOR.MINOR.PATCH e.g.: `1.3.2`.

### Milestones

* Milestones should have names according to the semantic format: MAJOR.MINOR.PATCH e.g.: `1.3.2`.

### Other rules

* Do not use rebase.