# Variables
make_directory=make
make_help=$(make_directory)/help.txt
make_entrypoint=$(make_directory)/entrypoint.sh

.SILENT: help
help:
	cat $(make_help)

# Composite
.SILENT: local
local: install run

.SILENT: docker
docker: docker-clean docker-build docker-run

.SILENT: git
git: git-fetch-prune git-pull-all

# Simple
## NPM
.SILENT: install
install:
	bash $(make_entrypoint) install

.SILENT: style
style:
	bash $(make_entrypoint) style

.SILENT: style-fix
style-fix:
	bash $(make_entrypoint) style-fix

.SILENT: run
run:
	bash $(make_entrypoint) run

.SILENT: build
build:
	bash $(make_entrypoint) build

## Docker
.SILENT: docker-build
docker-build:
	bash $(make_entrypoint) docker-build

.SILENT: docker-run
docker-run:
	bash $(make_entrypoint) docker-run

.SILENT: docker-clean
docker-clean:
	bash $(make_entrypoint) docker-clean

## Git
.SILENT: git-fetch-prune
git-fetch-prune:
	bash $(make_entrypoint) git-fetch-prune

.SILENT: git-pull-all
git-pull-all:
	bash $(make_entrypoint) git-pull-all