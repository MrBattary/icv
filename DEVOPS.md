# DevOps

## Files

Since this project is conceived as a small one, it belongs to some group. Therefore, the files necessary for CI/CD or
deployment are placed in the [devops](devops) directory.

## CI/CD Usage

### Deploy

There are two jobs `deploy-development` and `deploy-production`. Both requires variable with key `DEPLOY_VERSION` and
value in format `X.X.X` which should be specified with the actual version that available in the Package Registry.

### Release

There are auto versioning for the `develop` branch. This versioning only applies to PATCH versions.  
In order to create a MINOR or MAJOR version, you must:

1) Create MR from the `develop` branch to the `master` branch.
2) Set milestone for this MR e.g. `1.2.3`.
3) Provide MR title in the semantic format: MAJOR.MINOR.PATCH e.g.: `1.2.3`
4) Check the **squash commits** checkbox.

The `master` branch pipelines will trigger and create tag according to the MR title, e.g. for title `1.2.3` tag `1.2.3`
will be created and package `1.2.3` will be added to the package registry.

## CI/CD Setup

ICV project uses CI/CD practices.
To use CI/CD, further setup is required.

### Runners

The DinD (Docker in Docker) scheme is used for runners.
Therefore, when executed, the runners pulls up images from the image repository (e.g. hub.docker).

#### Optional

* Clean old `srv` directory: `rm -r /srv/gitlab-runner`.
* If the direct image repository is not available, provide mirrors:
    1) Open Docker configuration file `/etc/docker/daemon.json`.
    2) Add mirrors records:
  ```json
  {
    "registry-mirrors": [
        "https://dockerhub.timeweb.cloud",
        "https://mirror.gcr.io"
    ]
  }
  ```

#### Docker runner

1. Run container with a new
   runner: `docker run -d --name icv-docker-runner --restart always -v /srv/gitlab-runner/icv-docker-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest`.
2. Register a new docker
   runner: `docker run --rm -it -v /srv/gitlab-runner/icv-docker-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest register`.
    1) GitLab instance URL: `https://gitlab.com/`.
    2) Registration token available in **Runners** section of
       the [ICV project CI/CD settings](https://gitlab.com/michael-linker/icv/-/settings/ci_cd) page.
    3) Provide description, e.g `The ICV project docker runner`.
    4) Provide tags, e.g. `icv,docker`
    5) Provide optional maintenance note.
    6) Executor: `docker`.
    7) Provide main Docker image name from the [Dockerfile](Dockerfile), e.g. `node:20-alpine`.

#### System runner

1. Run container with a new
   runner: `docker run -d --name icv-system-runner --restart always -v /srv/gitlab-runner/icv-system-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest`.
2. Register a new system
   runner: `docker run --rm -it -v /srv/gitlab-runner/icv-system-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest register`.
    1) GitLab instance URL: `https://gitlab.com/`.
    2) Registration token available in **Runners** section of
       the [ICV project CI/CD settings](https://gitlab.com/michael-linker/icv/-/settings/ci_cd) page.
    3) Provide description, e.g `The ICV project system runner`.
    4) Provide tags, e.g. `icv,system`
    5) Provide optional maintenance note.
    6) Executor: `docker`.
    7) Provide main Docker image name, e.g. `ubuntu:22.04`.

### Environments

Environments can be created on the [ICV project environments](https://gitlab.com/michael-linker/icv/-/environments) page.

#### Development

Create `development` environment.

#### Production

Create `production` environment.

### Access Tokens

Required a system token with name e.g. `Gitlab Version Bot` for the CI/CD runner with the `api` privilege
and `mantainer` role.

### Variables

Variables can be created in the **Variables** section of
the [ICV project CI/CD settings](https://gitlab.com/michael-linker/icv/-/settings/ci_cd) page.

#### Shared variables

These variables required for any environment:

| KEY                | Example            | Description                                                                                                                                                                | Protected | Masked  | Expanded |
|--------------------|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|---------|----------|
| `CI_CD_TOKEN`      | `tOkEnVaLuE`       | Token value from the access token `Gitlab Version Bot`.                                                                                                                    | &#9744;   | &#9745; | &#9744;  |
| `KEEP_MAJOR`       | `2` or `ALL`       | How many MAJOR.X.X versions will be kept as tags and packages.                                                                                                             | &#9745;   | &#9744; | &#9744;  |
| `KEEP_MINOR`       | `3` or `ALL`       | How many X.MINOR.X versions will be kept as tags and packages.                                                                                                             | &#9745;   | &#9744; | &#9744;  |
| `KEEP_PATCH`       | `4` or `ALL`       | How many X.X.PATCH versions will be kept as tags and packages.                                                                                                             | &#9745;   | &#9744; | &#9744;  |
| `KEEP_HOURS`       | Number e.g. `72`   | How long jobs are stored in the repository. Do not set value to 0 (zero), otherwise jobs in the running pipelines will be erased, which will cause the pipelines to break. | &#9745;   | &#9744; | &#9744;  |
| `KEEP_JOBS`        | Number e.g. `1000` | How many jobs are stored in the repository. Do not set value too low, otherwise jobs in the running pipelines will be erased, which will cause the pipelines to break.     | &#9745;   | &#9744; | &#9744;  |
| `IS_PARTIAL_ERASE` | `true` or `false`  | Is the job erasure partial or complete. If partial, the erasure will be interrupted when the first erased job is encountered.                                              | &#9745;   | &#9744; | &#9744;  |

#### Environment variables

These variables required for `development` and `production` environments:

| KEY                  | Example                | Description                                                                                                                   | Protected | Masked  | Expanded |
|----------------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------|-----------|---------|----------|
| `DEPLOY_DIRECTORY`   | `/var/www/site`        | Directory where site files are stored.                                                                                        | &#9745;   | &#9745; | &#9744;  |
| `PUBLIC_RES_ARCHIVE` | `public.zip` or `NONE` | The name of the archive containing the public directory and the resources inside it. The `NONE` is preventing archive deploy. | &#9745;   | &#9744; | &#9744;  |
| `SSH_HOST`           | `1.2.3.4`              | Host ip address.                                                                                                              | &#9745;   | &#9745; | &#9744;  |
| `SSH_USER`           | `root`                 | User under whom ssh access will be performed .                                                                                | &#9745;   | &#9745; | &#9744;  |
| `SSH_PRIVATE_KEY`    | -                      | RSA private key. RSA public key should be available for `SSH_USER` in the `SSH_HOST`                                          | &#9745;   | &#9745; | &#9744;  |

### Secure Files

To install resources, you need an archive with resources having a hierarchy as [res/resources](res/resources).  
Copy the archive name to the `PUBLIC_RES_ARCHIVE` variable, and this archive will be delivered to the target machine
during deployment, and then unzipped so that ICV can use the resources from this directory.  
If the value of the `PUBLIC_RES_ARCHIVE` variable is `NONE`, then the archive is not required because it will not be
deployed.