#!/usr/bin/env sh

# Usage: script.sh $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

latest_version=$(curl -Ss --header "PRIVATE-TOKEN: $1" "$2/projects/$3/repository/tags" | jq -r '.[0] | .name')
echo "Creating ${latest_version} build"
# The custom variable is used because even after the 'npm run build' command fails, the script continues
build_successful=false
npm run build && build_successful=true
if [ "$build_successful" = false ]; then
  exit 1
fi
echo "$latest_version" > build/version.txt
cp -r res/resources build/
cd build
zip -r "build.zip" .
mv "build.zip" ../.