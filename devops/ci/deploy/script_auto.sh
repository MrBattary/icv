#!/bin/bash

# Usage: script.sh $SSH_USER $SSH_HOST $DEPLOY_DIRECTORY

ssh "$1@$2" "rm -rf $3/* && exit"
scp build.zip "$1@$2:$3/"
ssh "$1@$2" "cd $3 && unzip -qq build.zip && rm -f build.zip && exit"