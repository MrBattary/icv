#!/bin/bash

# Usage: script.sh $SSH_USER $SSH_HOST $DEPLOY_DIRECTORY $DEPLOY_VERSION $CI_JOB_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

ssh "$1@$2" "rm -rf $3/* && cd $3 && wget --header=\"JOB-TOKEN: $5\" $6/projects/$7/packages/generic/build/$4/$4.zip && unzip -qq $4.zip && rm -f $4.zip && exit"