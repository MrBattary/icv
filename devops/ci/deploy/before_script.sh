#!/bin/bash

# Usage: before_script.sh $SSH_PRIVATE_KEY $SSH_HOST

apt-get update -qq
apt-get install -yqq ssh jq curl

install -m 600 -D /dev/null ~/.ssh/id_rsa
echo "$1" | base64 -d > ~/.ssh/id_rsa
ssh-keyscan -H "$2" >> ~/.ssh/known_hosts