#!/bin/bash

# Usage: script.sh $SSH_USER $SSH_HOST $DEPLOY_DIRECTORY $RESOURCES_ARCHIVE $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

no_resources_archive_keyword="NONE"

if [ $4 = $no_resources_archive_keyword ]; then
  echo "Deployment of the resource archive is skipped."
else
  resources_archive_file_id=$(curl -Ss --header "PRIVATE-TOKEN: $5" "$6/projects/$7/secure_files" | jq ".[] | select(.name==\"$4\") | .id")
  ssh "$1@$2" "cd $3 && rm -rf resources && curl -Ss --request GET --header \"PRIVATE-TOKEN: $5\" $6/projects/$7/secure_files/$resources_archive_file_id/download --output resources.zip && unzip -qq resources.zip && rm -f resources.zip && exit"
fi