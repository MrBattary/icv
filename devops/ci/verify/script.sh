#!/bin/bash

# Usage: script.sh $CI_MERGE_REQUEST_TITLE $CI_MERGE_REQUEST_SQUASH_ON_MERGE $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

# Regex: NUMBER.NUMBER.NUMBER
is_merge_request_has_wrong_setup=false

echo "INFO: Checking that title '$1' of this merge request matches semantic naming conversation: MAJOR.MINOR.PATCH ..."
semantic_version_pattern='^(0|[1-9][0-9]*).(0|[1-9][0-9]*).(0|[1-9][0-9]*)$'
if [[ ! "$1" =~ $semantic_version_pattern ]];
then
    echo "ERROR: The title '$1' of the merge request should match semantic naming conversation: MAJOR.MINOR.PATCH!"
    is_merge_request_has_wrong_setup=true
else
    echo "OK"
fi

echo "INFO: Checking that the 'squash on merge' option is enabled for this merge request ..."
if [[ -z "${2+x}" || "$2" = false ]];
then
    echo "ERROR: The 'squash on merge' option should be enabled for the merge request!"
    is_merge_request_has_wrong_setup=true
else
    echo "OK"
fi

echo "INFO: Checking that the 'develop' branch is source of this merge request ..."
if [[ "$3" != "develop" ]];
then
    echo "ERROR: Only 'develop' branch allowed being merged to the 'master' branch!"
    is_merge_request_has_wrong_setup=true
else
    echo "OK"
fi

if [ "$is_merge_request_has_wrong_setup" = true ] ;
then
    echo "ERROR: Merge request has wrong setup, please fix errors and try again."
    exit 1
else
    echo "INFO: All checks have been passed. Merge request is ready to be merged to the master branch."
fi