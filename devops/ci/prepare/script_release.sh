#!/bin/bash

# Usage: script_release.sh $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

# Get second commit from branch, read this name (should be in the semantic version format X.X.X)
new_version=$(curl -sS --header "PRIVATE-TOKEN: $1" "$2/projects/$3/repository/commits?ref_name=master" | jq -r '.[1] | .title')
# Create new tag
created_version=$(curl -Ss -X POST --header "PRIVATE-TOKEN: $1" "$2/projects/$3/repository/tags?tag_name=$new_version&ref=master" | jq -r '.name')
echo "Tagged new version $created_version"