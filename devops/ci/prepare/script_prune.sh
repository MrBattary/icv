#!/bin/bash

# Usage: script_release.sh $KEEP_MAJOR $KEEP_MINOR $KEEP_PATCH $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

ci_private_token="$4"
ci_api_v4_url="$5"
ci_project_id="$6"

initial_versions_response=$(curl -Ss --request GET --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/repository/tags" |
 # The jq gets JSON array of objects and sorts them using the 'name' field.
 # These 'name' fields presented in the version format MAJOR.MINOR.PATCH (e.g. 1.0.9 and 1.1.10).
 # This version values splits by dots to numbers (e.g. 1.0.9 to 109, 1.1.10 to 1110) and compares to each other in the ascending order.
 # In result all JSON array object gets reversed to get descending order of objects with version 'name' fields.
 jq -r 'sort_by(.name|split(".")|map(tonumber)) | reverse | .[] | .name')
readarray -t initial_versions <<< $initial_versions_response
declare -a initial_versions

declare -a major_versions
declare -a minor_versions
declare -a patch_versions

for (( i = 0; i < "${#initial_versions[@]}"; i++ )); do
  version=( ${initial_versions[$i]//./ } )

  if [ ${version[1]} -eq 0 ] && [ ${version[2]} -eq 0 ]; then
    major_versions+=(${initial_versions[$i]})
  elif [ ${version[2]} -eq 0 ]; then
    minor_versions+=(${initial_versions[$i]})
  else
    patch_versions+=(${initial_versions[$i]})
  fi
done

_prune_versions() {
  local save_version_number="$1"
  shift
  local versions=("$@")
  echo "Keeping first $save_version_number versions from total ${#versions[@]} versions (${versions[@]})."
  for (( i = $save_version_number; i < "${#versions[@]}"; i++ )); do
    echo "Pruning: ${versions[$i]} version."
    curl -Ss --request DELETE --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/repository/tags/${versions[$i]}"
    prune_package_id=$(curl -Ss --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/packages" | jq -r ".[] | select(.version==\"${versions[$i]}\") | .id")
    curl -Ss --request DELETE --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/packages/$prune_package_id"
  done
}

keep_major="$1"
keep_minor="$2"
keep_patch="$3"
keep_all_keyword="ALL"

if [ $keep_major = $keep_all_keyword ]; then
  echo "Keeping all major versions."
else
  echo "Pruning some major versions except the $keep_major newest ones."
  _prune_versions $keep_major "${major_versions[@]}"
fi

if [ $keep_minor = $keep_all_keyword ]; then
  echo "Keeping all minor versions."
else
  echo "Pruning some minor versions except the $keep_minor newest ones."
  _prune_versions $keep_minor "${minor_versions[@]}"
fi

if [ $keep_patch = $keep_all_keyword ]; then
  echo "Keeping all patch versions."
else
  echo "Pruning some patch versions except the $keep_patch newest ones."
  _prune_versions $keep_patch "${patch_versions[@]}"
fi

unset ci_private_token
unset ci_api_v4_url
unset ci_project_id