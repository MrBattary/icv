#!/bin/bash

# Usage: script_patch.sh $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

# Get latest tag
initial_version=$(curl -Ss --header "PRIVATE-TOKEN: $1" "$2/projects/$3/repository/tags" | jq -r '.[0] | .name')
result_version=$(bash ./devops/ci/prepare/update_version.sh -p $initial_version | tail -n 1)
# Create new tag
created_version=$(curl -Ss -X POST --header "PRIVATE-TOKEN: $1" "$2/projects/$3/repository/tags?tag_name=$result_version&ref=develop" | jq -r '.name')
echo "Tagged new version $created_version"