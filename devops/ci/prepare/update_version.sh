#!/bin/bash

# Usage: update_version.sh <-M/-m/-p> <X.X.X>

readonly initial_version=$2
versions=()

_read_version_() {
  echo "Initial version: $initial_version"
  versions=( ${initial_version//./ } )
}

_print_version_() {
  echo "Result version: ${versions[0]}.${versions[1]}.${versions[2]}"
  echo "${versions[0]}.${versions[1]}.${versions[2]}"
}

_major() {
  _read_version_

  echo "Major version increment"
  ((versions[0]++))
  versions[1]=0
  versions[2]=0

  _print_version_
}

_minor() {
  _read_version_

  echo "Minor version increment"
  ((versions[1]++))
  versions[2]=0

  _print_version_
}

_patch() {
  _read_version_

  echo "Patch version increment"
  ((versions[2]++))

  _print_version_
}

case $1 in
  -M) _major;;
  -m) _minor;;
  -p) _patch;;
  *) exec "$@";;
esac