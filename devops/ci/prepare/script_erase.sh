#!/bin/bash

# Usage: script_erase.sh $KEEP_HOURS $KEEP_JOBS $IS_PARTIAL_ERASE $CI_CD_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

keep_hours="$1"
keep_hours_epoch=$(("$keep_hours" * 60 * 60))
epoch_now=$(date +%s)
keep_jobs="$2"
is_partial_erase="$3"
readonly is_partial_erase_keyword="true"
ci_private_token="$4"
ci_api_v4_url="$5"
ci_project_id="$6"

readonly jobs_per_page=100
jobs_page=1
current_job=1
while loaded_jobs=$(curl -Ss --request GET --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/jobs?page=$jobs_page&per_page=$jobs_per_page") && [ "$loaded_jobs" != "[]" ]; do
  loaded_jobs_count=$(echo "$loaded_jobs" | jq '. | length')
  for ((i = 0; i < "$loaded_jobs_count"; i++)); do
    loaded_job_id=$(echo "$loaded_jobs" | jq ".[$i].id")
    loaded_job_created_at_epoch=$(date -d $(echo "$loaded_jobs" | jq -r ".[$i].created_at") +%s)
    loaded_job_started_at=$(echo "$loaded_jobs" | jq ".[$i].started_at")
    loaded_job_erased_at=$(echo "$loaded_jobs" | jq ".[$i].erased_at")
    if [ "$loaded_job_erased_at" != "null" ]; then
      echo "Job $loaded_job_id has already been erased."
      if [ "$is_partial_erase" = "$is_partial_erase_keyword" ]; then
        echo "Partial erasure was interrupted because an already erased job was encountered."
        unset ci_private_token
        unset ci_api_v4_url
        unset ci_project_id
        exit 0
      fi
    elif [ $((epoch_now - loaded_job_created_at_epoch)) -lt $keep_hours_epoch ]; then
      echo "Job $loaded_job_id was created less than $keep_hours hours ago and is protected from erasure."
    elif [ $current_job -le $keep_jobs ]; then
      echo "Job $loaded_job_id is $current_job of $keep_jobs jobs that are protected from erasure."
    elif [ "$loaded_job_started_at" != "null" ]; then
      echo -n "Erasing job $loaded_job_id ..."
      erasure_response_code=$(curl -Ss -o /dev/null -w "%{http_code}" --request POST --header "PRIVATE-TOKEN: $ci_private_token" "$ci_api_v4_url/projects/$ci_project_id/jobs/$loaded_job_id/erase")
      if [ "$erasure_response_code" == "201" ]; then
        echo " done."
      else
        echo " failed with response code $erasure_response_code."
      fi
    fi
    current_job=$((current_job + 1))
  done
  jobs_page=$((jobs_page + 1))
done

unset ci_private_token
unset ci_api_v4_url
unset ci_project_id