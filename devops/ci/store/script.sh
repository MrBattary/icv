#!/bin/bash

# Usage: script.sh $CI_JOB_TOKEN $CI_API_V4_URL $CI_PROJECT_ID

version=$(unzip -p build.zip version.txt | cat)
store_response_code=$(curl -Ss -o /dev/null -w "%{http_code}" --header "JOB-TOKEN: $1" --upload-file build.zip "$2/projects/$3/packages/generic/build/$version/$version.zip")

if [ "$store_response_code" = "201" ]; then
  echo "Created package for version $version"
else
  echo "Failed to create package $version"
  exit 1
fi