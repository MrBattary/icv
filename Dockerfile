# Global ARG
ARG PROJECT_DIRECTORY=/tmp/icv

# 1: Setup application dependencies
FROM node:20-alpine AS dependencies

ARG PROJECT_DIRECTORY

WORKDIR $PROJECT_DIRECTORY

# Prepare production dependencies configuration file
COPY package.json .

# Install production dependencies
RUN npm install

# 2: Build application
FROM dependencies AS build

# Prepare project files
COPY . .

# Production build
RUN npm run build

# Copy static resources
RUN cp -rf res/* build

# 3: Setup Nginx
FROM nginx:alpine AS nginx

ARG PROJECT_DIRECTORY

# Remove default Nginx config
RUN rm -rf /etc/nginx/conf.d/*

# Override default Nginx config with application custom
COPY --from=build $PROJECT_DIRECTORY/docker/local.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# 4: Run Nginx with built application
FROM nginx AS run

# Copy built application to nginx web folder
COPY --from=build $PROJECT_DIRECTORY/build .

# Expose HTTP port according to Nginx config
EXPOSE 80

# Containers run Nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]