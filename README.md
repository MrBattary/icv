# ICV(Interactive Curriculum Vitae)

## Description

The Interactive Curriculum Vitae project allows you to create a more informative resume than a paper one.

There is an opportunity not only to show projects or teams that you have worked with or are working with right
now, but also to make links to them.

By specifying personal information and projects in which you have participated, the system will determine your
experience and acquired skills on its own.

## How to run

### Make

To use Make it is required to install [GNU Make](https://www.gnu.org/software/make/) with [Docker](#docker)
and [NPM](#npm).

| Command               | Description                                                                                                                                                                                            |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `make` or `make help` | Get help on the available commands.                                                                                                                                                                    |
| `make local`          | Install the required dependencies locally and run the app in the development mode on [http://localhost:3000](http://localhost:3000).                                                                   |
| `make docker`         | Remove the old ICV docker image, build a new one, and run an auto removable container with nginx and a production application on 80 port which is accessible via [http://localhost](http://localhost). |

### Docker

To run this web-application via [Docker](https://www.docker.com/):

1) Build image: `docker build -t icv .`.
2) Run container *(with nginx port)*: `docker run --rm --name icv-nginx -p 80:80 icv`.
3) Open `localhost` in the browser.

### NPM

To use NPM scripts it is required to install [NPM](https://www.npmjs.com/) and install dependencies with the
command `npm install`.

| Command             | Description                                                                             |
|---------------------|-----------------------------------------------------------------------------------------|
| `npm start`         | Runs the app in the development mode on [http://localhost:3000](http://localhost:3000). |
| `npm test`          | Launches the test runner in the interactive watch mode.                                 |
| `npm run build`     | Builds the app for production to the `build` folder.                                    |
| `npm run style`     | Check's a code style.                                                                   |
| `npm run style:fix` | Check's a code style then trying to correct all misspellings.                           |

## Contributing

Contribution guidelines rules available in the [CONTRIBUTING.md](CONTRIBUTING.md).

## DevOps

DevOps practices available in the [DEVOPS.md](DEVOPS.md).

## Authors

Michael Linker - *Entire work* - [work.michaellinker@gmail.com](mailto:work.michaellinker@gmail.com)