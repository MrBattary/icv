#!/usr/bin/env bash

set +x

# Variables
## General
readonly log_script_name='[ICV]'
## Colors
readonly log_name_color="\033[0;96m"
readonly log_no_color="\033[0m"
readonly log_info_color="\033[0;92m"
readonly log_warning_color="\033[0;93m"
readonly log_error_color="\033[0;91m"
## Patterns
readonly log_name="${log_name_color}${log_script_name}${log_no_color}"
readonly log_datetime_format="+%Y-%m-%d %H:%M:%S"
readonly log_level_info="${log_info_color}INFO${log_no_color}"
readonly log_level_warning="${log_warning_color}WARN${log_no_color}"
readonly log_level_error="${log_error_color}ERR${log_no_color}"
readonly log_pattern="\${log_name} \$(date '${log_datetime_format}') \$1 : \$2"


# Functions
_log_general_() {
  eval echo -e "$log_pattern"
}

_log_i_() {
  _log_general_ "$log_level_info" "$1"
}

_log_w_() {
  _log_general_ "$log_level_warning" "$1"
}

_log_e_() {
  _log_general_ "$log_level_error" "$1"
}