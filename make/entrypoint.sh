#!/usr/bin/env bash

set +x

source "$(dirname "$0")/docker-utils.sh"
source "$(dirname "$0")/variables.sh"

# Main functions
## NPM
_install() {
  _log_i_ "Installing dependencies..."
  npm install
  _log_i_ "Dependencies has been installed."
}

_style() {
  _log_i_ "Checking code style..."
  npm run style
  _log_i_ "Code style has been checked."
}

_style_fix() {
  _log_i_ "Checking and fixing code style..."
  npm run style:fix
  _log_i_ "Code style has been checked and fixed."
}

_run() {
  _log_i_ "Starting ICV development server..."
  npm run start
  _log_i_ "ICV development server has been finished."
}

_build() {
  _log_i_ "Building ICV..."
  npm run build
  _log_i_ "ICV has been built."
}

## Docker
_docker_build() {
  _log_i_ "Building ICV docker image..."
  docker build -t ${docker_image} .
  _log_i_ "ICV docker image has been built."
}

_docker_run() {
  _log_i_ "ICV docker container starting on http://localhost ..."
  docker run --rm --name ${docker_container} -p 80:80 ${docker_image}
  _log_i_ "ICV docker container has been finished."
}

_docker_clean() {
  _log_i_ "ICV docker image cleaning..."
  _is_image_exists "${docker_image}"
  local is_image_exists=$?
  if [ "$is_image_exists" -eq 0 ]; then
    docker image rm ${docker_image}
    _log_i_ "ICV docker image has been cleaned."
  else
    _log_w_ "ICV docker image is already cleaned."
  fi
}

## Git
_git_fetch_prune() {
  _log_i_ "Fetching and pruning outdated git branches and tags..."
  git fetch --prune --prune-tags
  _log_i_ "Outdated git branches and tags has been pruned and local repo has been fetched."
}

_git_pull_all() {
  _log_i_ "Pulling all remote git branches..."
  local remote_git_source=$(git remote -v | grep '(fetch)' | awk '{print $1}')
  local initial_git_branch=$(git branch --show-current)
  # grep: Matches the inverse of given string
  # sed : Removes control sequences, \x1B matches ESC
  git branch -r \
    | grep -v '\->' \
    | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" \
    | while read remote; do \
        local git_current_branch="${remote#origin/}"
        _log_i_ "Pulling the '${git_current_branch}' branch from the '${remote_git_source}'..."
        git switch --quiet "${git_current_branch}"
        git pull "${remote_git_source}" "${git_current_branch}"
      done
  git switch --quiet "${initial_git_branch}"
  _log_i_ "All remote git branches has been pulled."
}

# Executor
case $1 in
install ) _install ;;
style ) _style ;;
style-fix ) _style_fix ;;
run ) _run ;;
build ) _build ;;
docker-build) _docker_build ;;
docker-run) _docker_run ;;
docker-clean) _docker_clean ;;
git-fetch-prune ) _git_fetch_prune ;;
git-pull-all ) _git_pull_all ;;
*) exec "$@" ;;
esac
