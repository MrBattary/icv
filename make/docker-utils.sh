#!/usr/bin/env bash

set +x

source "$(dirname "$0")/log-utils.sh"

# Usage: _is_image_exists $image_tag
# Returns: 0 if exists, 1 otherwise
_is_image_exists() {
  local image_tag="$1"
  _log_i_ "Checking if the image with tag $image_tag exists..."
  if [ "$( docker images -q $image_tag:latest | wc -w )" -gt 0 ]; then
    _log_i_ "Image $image_tag exists."
    return 0
  else
    _log_w_ "Image $image_tag does not exist!"
    return 1
  fi
}

# Usage: _is_container_exists $container_name
# Returns: 0 if exists, 1 otherwise
_is_container_exists() {
  local container_name="$1"
  _log_i_ "Checking if the container $container_name exists..."
  if [ "$( docker ps -a | grep -c "$container_name" )" -gt 0 ]; then
    _log_i_ "Container $container_name exists."
    return 0
  else
    _log_w_ "Container $container_name does not exist!"
    return 1
  fi
}

# Usage: _is_container_running $container_name
# Returns: 0 if exists, 1 otherwise
_is_container_running() {
  local container_name="$1"
  _is_container_exists "$container_name"
  local is_exists=$?
  if [ $is_exists -eq 0 ]; then
    _log_i_ "Checking if the container $container_name is running..."
    if [ "$(docker ps -q -f status=running -f name="$container_name")" ]; then
      _log_i_ "Container $container_name is running."
      return 0
    else
      _log_w_ "Container $container_name is not running!"
      return 1
    fi
  else
    return 1
  fi
}
