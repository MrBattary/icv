// Replace the cvReducer:initialState with this state during local development.
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const initialState: CvStore = {
  cvUser: {
    initial: {
      flow: [],
      value: {
        id: 'u0',
        avatar: {
          src: '/public/media/avatar.jpg',
          alt: 'Nullam metus tortor',
        },
        firstName: 'Lorem',
        middleName: 'Nullam quis odio',
        lastName: 'Ipsum',
        age: 999,
        birthdate: {
          year: 0,
          month: 7,
          day: 2,
        },
        position: {
          id: 'pos0',
          name: 'Position 0',
          color: '#007700',
          specialization: {
            id: 'spec0',
            name: 'Specialization 0',
            color: '#003366',
          },
        },
        location: {
          name: 'Nunc laoreet sodales metus',
          mapping: {
            latitude: 59.936356,
            longitude: 30.302233,
            placeId: 'ChIJXRSiaRkxlkYR6naWIrXNs3I',
          },
        },
        social: {
          contacts: [
            { type: 'email', value: 'lorem.ipsum@mail.test' },
            { type: 'telegram', value: '@lorem.ipsum' },
          ],
          links: [
            { type: 'gitlab', path: '/test' },
            { type: 'github', path: '/test' },
            { type: 'linkedin', path: '/test' },
          ],
        },
        preferences: {
          contacts: 'active',
          tagGroups: [
            {
              name: 'Tag Group 0',
              tags: [
                {
                  id: 'tag1',
                  name: 'Tag 1',
                },
              ],
            },
            {
              name: 'Tag Group 1',
              tags: [
                {
                  id: 'tag1',
                  name: 'Tag 1',
                },
                {
                  id: 'tag2',
                  name: 'Tag 2',
                },
              ],
            },
          ],
        },
      },
    },
    actual: undefined,
  },
  cvActivities: {
    initial: {
      flow: [],
      value: [
        {
          id: 'p0',
          type: 'startup',
          period: {
            start: {
              year: 2020,
              month: 3,
              day: 3,
            },
            end: {
              year: 2021,
              month: 5,
              day: 1,
            },
          },
          position: {
            id: 'pos0',
            name: 'Position 0',
            color: '#007700',
            specialization: {
              id: 'spec0',
              name: 'Specialization 0',
              color: '#003366',
            },
          },
          project: {
            id: 'prj0',
            name: 'Project 0',
            links: [
              {
                type: 'github',
                path: '/github',
              },
            ],
          },
          subactivities: [
            {
              id: 'sa0',
              period: {
                start: {
                  year: 2020,
                  month: 0,
                  day: 3,
                },
                end: {
                  year: 2020,
                  month: 11,
                  day: 15,
                },
              },
              description: '',
              skills: [
                {
                  id: 'skl1',
                  name: 'Skill 1',
                  tags: [
                    {
                      id: 'tag1',
                      name: 'Tag 1',
                    },
                  ],
                },
                {
                  id: 'skl0',
                  name: 'Skill 0',
                },
                {
                  id: 'skl2',
                  name: 'Skill 2',
                  tags: [
                    {
                      id: 'tag2',
                      name: 'Tag 2',
                    },
                  ],
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
                {
                  id: 'skl6',
                  name: 'Skill 6',
                },
              ],
            },
            {
              id: 'sa1',
              period: {
                start: {
                  year: 2021,
                  month: 0,
                  day: 10,
                },
                end: {
                  year: 2021,
                  month: 5,
                  day: 15,
                },
              },
              skills: [
                {
                  id: 'skl11',
                  name: 'Skill 11',
                },
                {
                  id: 'skl2',
                  name: 'Skill 2',
                  tags: [
                    {
                      id: 'tag2',
                      name: 'Tag 2',
                    },
                  ],
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
                {
                  id: 'skl5',
                  name: 'Skill 555555555 555555 55555555555555 55555555555555',
                },
              ],
            },
          ],
        },
        {
          id: 'c0',
          type: 'commercial',
          period: {
            start: {
              year: 2021,
              month: 8,
              day: 5,
            },
            end: 'present',
          },
          position: {
            id: 'pos1',
            name: 'Position 1',
            color: '#cc9933',
            specialization: {
              id: 'spec1',
              name: 'Specialization 1',
              color: '#333399',
            },
          },
          project: {
            id: 'prj2',
            name: 'Project 1',
            links: [
              {
                type: 'common',
                url: 'https://gitlab.com',
              },
              {
                type: 'gitlab',
                path: '/gitlab-org/gitlab',
              },
            ],
            avatar: {
              src: '/public/media/gitlab-avatar.png',
              alt: 'Gitlab Avatar',
            },
          },
          provider: {
            id: 'prov0',
            name: 'Provider collective',
            link: {
              url: 'https://gitlab.com/gitlab',
            },
            avatar: {
              src: '/public/media/gitlab-avatar.png',
              alt: 'Gitlab Avatar',
            },
          },
          subactivities: [
            {
              id: 'sa2',
              period: {
                start: {
                  year: 2020,
                  month: 8,
                  day: 5,
                },
                end: 'present',
              },
              description:
                'Some test description of subactivity 2. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla in tortor non ipsum commodo mattis in at massa. Nam scelerisque lobortis vulputate.',
              skills: [
                {
                  id: 'skl1',
                  name: 'Skill 1',
                  tags: [
                    {
                      id: 'tag1',
                      name: 'Tag 1',
                    },
                  ],
                },
                {
                  id: 'skl6',
                  name: 'Skill 6',
                },
                {
                  id: 'skl0',
                  name: 'Skill 0',
                },
                {
                  id: 'skl2',
                  name: 'Skill 2',
                  tags: [
                    {
                      id: 'tag2',
                      name: 'Tag 2',
                    },
                  ],
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
              ],
            },
          ],
        },
        {
          id: 'e1',
          type: 'education',
          period: {
            start: {
              year: 2020,
              month: 9,
              day: 1,
            },
            end: {
              year: 2022,
              month: 7,
              day: 30,
            },
          },
          position: {
            id: 'pos3',
            name: 'Position 3',
            color: '#880000',
            specialization: {
              id: 'spec1',
              name: 'Specialization 1',
              color: '#333399',
            },
          },
          provider: {
            id: 'prov2',
            name: 'Provider 2 - Educational',
          },
          subactivities: [
            {
              id: 'sa3',
              period: {
                start: {
                  year: 2020,
                  month: 3,
                  day: 3,
                },
                end: {
                  year: 2020,
                  month: 10,
                  day: 13,
                },
              },
              description:
                'Some test description of subactivity 3. Morbi tristique diam eget elementum iaculis. In ultrices, ante quis molestie convallis, justo enim sollicitudin lacus, a dictum dolor metus eu odio. ',
              skills: [
                {
                  id: 'skl1',
                  name: 'Skill 1',
                  tags: [
                    {
                      id: 'tag1',
                      name: 'Tag 1',
                    },
                  ],
                },
                {
                  id: 'skl0',
                  name: 'Skill 0',
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
              ],
            },
            {
              id: 'sa4',
              period: {
                start: {
                  year: 2020,
                  month: 8,
                  day: 1,
                },
                end: {
                  year: 2022,
                  month: 5,
                  day: 25,
                },
              },
              description:
                'Some test description of subactivity 4. Ut non lacus semper, volutpat elit in, pellentesque erat. In hac habitasse platea dictumst.',
              skills: [
                {
                  id: 'skl0',
                  name: 'Skill 0',
                },
                {
                  id: 'skl10',
                  name: 'Skill 10',
                },
                {
                  id: 'skl11',
                  name: 'Skill 11',
                },
                {
                  id: 'skl2',
                  name: 'Skill 2',
                  tags: [
                    {
                      id: 'tag2',
                      name: 'Tag 2',
                    },
                  ],
                },
              ],
            },
            {
              id: 'sa5',
              period: {
                start: {
                  year: 2020,
                  month: 8,
                  day: 1,
                },
                end: 'present',
              },
              description:
                'Some test description of subactivity 5. In ultrices, ante quis molestie convallis, justo enim sollicitudin lacus, a dictum dolor metus eu odio. Ut non lacus semper, volutpat elit in, pellentesque erat.',
              skills: [
                {
                  id: 'skl11',
                  name: 'Skill 11',
                },
                {
                  id: 'skl2',
                  name: 'Skill 2',
                  tags: [
                    {
                      id: 'tag2',
                      name: 'Tag 2',
                    },
                  ],
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
                {
                  id: 'skl1',
                  name: 'Skill 1',
                  tags: [
                    {
                      id: 'tag1',
                      name: 'Tag 1',
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          id: 'e0',
          heirId: 'e1',
          type: 'education',
          period: {
            start: {
              year: 2016,
              month: 9,
              day: 1,
            },
            end: {
              year: 2020,
              month: 7,
              day: 1,
            },
          },
          position: {
            id: 'pos2',
            name: 'Position 2',
            color: '#ff99ff',
            specialization: {
              id: 'spec0',
              name: 'Specialization 0',
              color: '#003366',
            },
          },
          provider: {
            id: 'prov2',
            name: 'Provider 2 - Educational',
          },
          subactivities: [
            {
              id: 'sa6',
              period: {
                start: {
                  year: 2016,
                  month: 9,
                  day: 1,
                },
                end: {
                  year: 2020,
                  month: 7,
                  day: 1,
                },
              },
              description:
                'Some test description of subactivity 4 with heir of subactivity 3. Morbi tristique diam eget elementum iaculis. In ultrices, ante quis molestie convallis, justo enim sollicitudin lacus, a dictum dolor metus eu odio. ',
              skills: [
                {
                  id: 'skl1',
                  name: 'Skill 1',
                  tags: [
                    {
                      id: 'tag1',
                      name: 'Tag 1',
                    },
                  ],
                },
                {
                  id: 'skl0',
                  name: 'Skill 0',
                },
                {
                  id: 'skl3',
                  name: 'Skill 3',
                },
              ],
            },
          ],
        },
      ],
    },
    actual: undefined,
  },
  filters: {
    available: {
      loading: true,
      value: [],
    },
    applied: {
      loading: false,
      value: [
        {
          id: 1,
          type: 'ACTIVITY_TYPE',
          data: 'commercial',
        },
      ],
    },
  },
};
