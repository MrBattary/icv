import { ComponentProps } from '../../ComponentProps';

type DialogMoleculeProps = ComponentProps & {
  open: boolean;
  handleClose: () => void;
};

export default DialogMoleculeProps;
