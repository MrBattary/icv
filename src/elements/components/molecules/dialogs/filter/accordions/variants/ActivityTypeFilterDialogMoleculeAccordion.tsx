import React, { useEffect } from 'react';

import { AccordionDetails, Box } from '@mui/material';

import useI18n from '../../../../../../../hooks/internationalization/useI18n';

import mapActivityTypeToLocaleMapping from '../../../../../../../tools/internationalization/i18n/localization/namespaces/molecules/mappings/mapActivityTypeToLocaleMapping';

import ActivityTypeFilterData from '../../../../../../../types/business/filter/data/variants/type/ActivityTypeFilterData';
import ActivityType from '../../../../../../../types/business/activity/ActivityType';

import FilterDialogMoleculeAccordionProps from '../FilterDialogMoleculeAccordionProps';
import CheckboxAtom from '../../../../../atoms/checkboxes/CheckboxAtom';
import CheckboxGroupAtom from '../../../../../atoms/checkboxes/group/CheckboxGroupAtom';
import BaseFilterDialogMoleculeAccordion from '../base/BaseFilterDialogMoleculeAccordion';

import { ACTIVITY_TYPE_FILTER } from '../../../../../../../types/business/filter/data/prototype/fields/FilterType';
import { MOLECULES_LOCALE_NAMESPACE } from '../../../../../../../tools/internationalization/i18n/localization/namespaces/molecules/MoleculesLocaleNamespace';
import { DIALOG_FILTER_ACCORDION_ACTIVITY_TYPE_TITLE_MAPPING } from '../../../../../../../tools/internationalization/i18n/localization/namespaces/molecules/MoleculesLocaleNamespaceMappings';

type ActivityTypeFilterDialogMoleculeAccordionProps =
  FilterDialogMoleculeAccordionProps<ActivityTypeFilterData>;

const ActivityTypeFilterDialogMoleculeAccordion: React.FC<
  ActivityTypeFilterDialogMoleculeAccordionProps
> = ({
  className,
  isExpanded,
  initialFilterData,
  currentFilterData,
  onChange,
  onChangeExpand,
}) => {
  const { t } = useI18n(MOLECULES_LOCALE_NAMESPACE);

  const [accordionFilterData, setAccordionFilterData] = React.useState<
    ActivityTypeFilterData[]
  >([]);

  useEffect(() => {
    setAccordionFilterData(currentFilterData);
  }, [currentFilterData]);

  const resetFilter = () => {
    setAccordionFilterData([]);
    onChange([]);
  };

  const handleClick = (activityType: ActivityType) => {
    const newFilterData = [...accordionFilterData];

    const filterDataPosition = newFilterData.findIndex(
      (filterData) => filterData.data === activityType,
    );
    if (filterDataPosition === -1) {
      newFilterData.push({
        type: ACTIVITY_TYPE_FILTER,
        data: activityType,
      });
    } else {
      newFilterData.splice(filterDataPosition, 1);
    }

    setAccordionFilterData(newFilterData);
    onChange(newFilterData);
  };

  const renderCheckbox = (activityType: ActivityType) => (
    <Box
      className={`${activityType}-activity-type-checkbox__wrapper`}
      key={activityType}
      onClick={(e) => {
        e.stopPropagation();
        e.preventDefault();
        handleClick(activityType);
      }}>
      <CheckboxAtom
        className={`${activityType}-activity-type-checkbox`}
        name={activityType}
        checked={accordionFilterData.every(
          (filterData) => filterData.data !== activityType,
        )}
        label={t(mapActivityTypeToLocaleMapping(activityType))}
      />
    </Box>
  );

  const renderAvailableCheckboxes = () =>
    initialFilterData.map((filterData) => renderCheckbox(filterData.data));

  return (
    <BaseFilterDialogMoleculeAccordion
      className={`${className} activity-type-accordion`}
      id='activity-type'
      isExpanded={isExpanded}
      title={t(DIALOG_FILTER_ACCORDION_ACTIVITY_TYPE_TITLE_MAPPING)}
      data={currentFilterData}
      onChangeExpand={onChangeExpand}
      onReset={resetFilter}>
      <AccordionDetails className='activity-type-accordion__details'>
        <CheckboxGroupAtom className='details__group' row>
          {renderAvailableCheckboxes()}
        </CheckboxGroupAtom>
      </AccordionDetails>
    </BaseFilterDialogMoleculeAccordion>
  );
};

export default ActivityTypeFilterDialogMoleculeAccordion;
