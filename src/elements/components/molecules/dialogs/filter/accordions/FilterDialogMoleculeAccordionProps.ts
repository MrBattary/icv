import FilterData from '../../../../../../types/business/filter/data/FilterData';

import { ComponentProps } from '../../../../ComponentProps';

type FilterDialogMoleculeAccordionProps<T extends FilterData> =
  ComponentProps & {
    isExpanded: boolean;
    initialFilterData: T[];
    currentFilterData: T[];
    onChange: (filterData: T[]) => void;
    onChangeExpand: () => void;
  };

export default FilterDialogMoleculeAccordionProps;
