import React from 'react';

export type ComponentProps = {
  className?: string;
  key?: string | number | bigint;
  ref?: React.Ref<unknown>;
};
