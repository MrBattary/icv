import React from 'react';

import { Switch, SwitchProps } from '@mui/material';

type SwitchAtomProps = Omit<SwitchProps, 'icon' | 'checkedIcon'>;

/*
 The main difference between switch and toggle is that toggle provides two equal options,
 while switch provides a single option with on/off state.
 */
const SwitchAtom: React.FC<SwitchAtomProps> = ({
  className,
  checked,
  sx,
  onChange,
}) => (
  <Switch
    className={className}
    checked={checked}
    inputProps={{ 'aria-label': 'controlled' }}
    sx={sx}
    onChange={onChange}
  />
);

export default SwitchAtom;
