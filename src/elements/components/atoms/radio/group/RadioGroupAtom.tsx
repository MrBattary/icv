import React from 'react';

import { FormControl, RadioGroup } from '@mui/material';

type RadioGroupAtomProps = {
  className?: string;
  value?: unknown;
  defaultValue?: unknown;
  row?: boolean;
  children: React.ReactNode;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const RadioGroupAtom: React.FC<RadioGroupAtomProps> = ({
  className,
  value,
  defaultValue,
  row,
  children,
  onChange,
}) => (
  <FormControl className={`${className} form-control`}>
    <RadioGroup
      className='form-control__group'
      value={value}
      defaultValue={defaultValue}
      row={row}
      onChange={onChange}>
      {children}
    </RadioGroup>
  </FormControl>
);

export default RadioGroupAtom;
