import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { ButtonProps, IconButton } from '@mui/material';

import buildUrlFromEndpoint from '../../../../../tools/navigation/utils/buildUrlFromEndpoint';
import NavEndpoint from '../../../../../tools/navigation/endpoints/NavEndpoint';

export type LinkIconButtonAtomProps = ButtonProps & {
  to: NavEndpoint;
  replace?: boolean;
};

const LinkIconButtonAtom: React.FC<LinkIconButtonAtomProps> = ({
  className,
  id,
  children,
  to,
  replace = false,
}) => (
  <IconButton
    className={className}
    id={id}
    component={RouterLink}
    replace={replace}
    to={buildUrlFromEndpoint(to)}>
    {children}
  </IconButton>
);

export default LinkIconButtonAtom;
