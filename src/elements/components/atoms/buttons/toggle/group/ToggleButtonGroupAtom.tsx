import React from 'react';

import { ToggleButtonGroup, ToggleButtonGroupProps } from '@mui/material';

type ToggleButtonGroupAtomProps = ToggleButtonGroupProps;

const ToggleButtonGroupAtom: React.FC<ToggleButtonGroupAtomProps> = ({
  className,
  value,
  exclusive,
  size,
  fullWidth,
  color,
  sx,
  onChange,
  children,
}) => (
  <ToggleButtonGroup
    className={className}
    value={value}
    exclusive={exclusive}
    size={size}
    fullWidth={fullWidth}
    color={color}
    sx={sx}
    onChange={onChange}>
    {children}
  </ToggleButtonGroup>
);

export default ToggleButtonGroupAtom;
