import React from 'react';

import { ToggleButton, ToggleButtonProps } from '@mui/material';

type ToggleButtonAtomProps = ToggleButtonProps;

const ToggleButtonAtom: React.FC<ToggleButtonAtomProps> = ({
  className,
  value,
  sx,
  children,
}) => (
  <ToggleButton className={className} value={value} sx={sx}>
    {children}
  </ToggleButton>
);

export default ToggleButtonAtom;
