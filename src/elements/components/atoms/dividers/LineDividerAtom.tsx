import React from 'react';

import { Divider } from '@mui/material';
import { DividerProps } from '@mui/material/Divider/Divider';

const LineDividerAtom: React.FC<DividerProps> = ({
  className,
  flexItem = false,
  orientation = 'horizontal',
  variant = 'fullWidth',
  sx,
}) => (
  <Divider
    className={className}
    flexItem={flexItem}
    orientation={orientation}
    variant={variant}
    sx={sx}
  />
);

export default LineDividerAtom;
