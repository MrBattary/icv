import React from 'react';

type ImageAtomProps = {
  className?: string;
  width?: string;
  height?: string;
  src: string;
  srcError?: string;
  alt: string;
  altError?: string;
  loading?: 'lazy' | 'eager';
  style?: React.CSSProperties;
};

export const ImageAtom: React.FC<ImageAtomProps> = ({
  className,
  width,
  height,
  src,
  srcError,
  alt,
  altError = 'The image was not uploaded due to an error.',
  loading = 'lazy',
  style = { verticalAlign: 'bottom' },
}) => (
  <img
    className={className}
    width={width}
    height={height}
    loading={loading}
    onError={({ currentTarget }) => {
      currentTarget.onerror = null;
      currentTarget.src = srcError ?? '';
      currentTarget.alt = altError;
      currentTarget.className = `${currentTarget.className} error-image`;
    }}
    src={src}
    alt={alt}
    style={style}
  />
);

export default ImageAtom;
