import React from 'react';

type AppProps = {
  children?: React.ReactNode;
};

export default AppProps;
