import React from 'react';
import { Outlet } from 'react-router-dom';

import { CssBaseline, ThemeProvider } from '@mui/material';

import useAppTheme from '../../../hooks/layout/theme/useAppTheme';
import useMountEffect from '../../../hooks/wrapped/useMountEffect';

import AppProps from '../AppProps';
import useAppThemeUtils from '../../../hooks/layout/theme/useAppThemeUtils';

const AppTheme: React.FC<AppProps> = ({ children = <Outlet /> }) => {
  const { appTheme } = useAppTheme();
  const { applySystemThemeMode } = useAppThemeUtils();

  useMountEffect(() => {
    applySystemThemeMode();
  });

  return (
    <ThemeProvider theme={appTheme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default AppTheme;
