import i18n, { I18N_DEFAULT_LANGUAGE } from '../i18n';

const getI18nLanguage = () => i18n.resolvedLanguage || I18N_DEFAULT_LANGUAGE;

export default getI18nLanguage;
