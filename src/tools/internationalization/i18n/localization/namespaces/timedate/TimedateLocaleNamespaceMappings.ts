export const DATES_PRESENT_MAPPING = 'dates.present';

export type TimedateLocaleNamespaceMappings = typeof DATES_PRESENT_MAPPING;
