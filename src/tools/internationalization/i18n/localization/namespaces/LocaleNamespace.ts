import { LOCALE_NAMESPACE_NAME } from './LocaleNamespaceName';
import { OrganismsLocaleNamespaceMappings } from './organisms/OrganismsLocaleNamespaceMappings';
import { AssetsLocaleNamespaceMappings } from './assets/AssetsLocaleNamespaceMappings';
import { MoleculesLocaleNamespaceMappings } from './molecules/MoleculesLocaleNamespaceMappings';
import { TimedateLocaleNamespaceMappings } from './timedate/TimedateLocaleNamespaceMappings';
import { AppLocaleNamespaceMappings } from './app/AppLocaleNamespaceMappings';

export type LocaleNamespaceMappings =
  | AppLocaleNamespaceMappings
  | OrganismsLocaleNamespaceMappings
  | MoleculesLocaleNamespaceMappings
  | AssetsLocaleNamespaceMappings
  | TimedateLocaleNamespaceMappings;

/**
 * @typeParam T: Literal type of the namespace mappings.
 * @param name: Name of the namespace from the LOCALE_NAMESPACE_NAME enum.
 * @param mappings: Namespace mappings of type M.
 * Default value is unused and any value from the literal type M can be used.
 */
export type LocaleNamespace<T extends LocaleNamespaceMappings> = {
  name: LOCALE_NAMESPACE_NAME;
  mappings: T;
};
