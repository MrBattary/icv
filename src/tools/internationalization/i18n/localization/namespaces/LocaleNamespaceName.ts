export enum LOCALE_NAMESPACE_NAME {
  APP = 'app',
  ORGANISMS = 'organisms',
  MOLECULES = 'molecules',
  TIMEDATE = 'timedate',
  ASSETS = 'assets',
}
