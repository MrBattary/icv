export const NAME_MAPPING = 'name';

export type AppLocaleNamespaceMappings = typeof NAME_MAPPING;
