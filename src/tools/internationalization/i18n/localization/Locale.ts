import { LOCALE_NAMESPACE_NAME } from './namespaces/LocaleNamespaceName';
import { AppLocaleNamespace } from './namespaces/app/AppLocaleNamespace';
import { OrganismsLocaleNamespace } from './namespaces/organisms/OrganismsLocaleNamespace';
import { AssetsLocaleNamespace } from './namespaces/assets/AssetsLocaleNamespace';
import { MoleculesLocaleNamespace } from './namespaces/molecules/MoleculesLocaleNamespace';
import { TimedateLocaleNamespace } from './namespaces/timedate/TimedateLocaleNamespace';

export type Locale = {
  [LOCALE_NAMESPACE_NAME.APP]: AppLocaleNamespace;
  [LOCALE_NAMESPACE_NAME.ORGANISMS]: OrganismsLocaleNamespace;
  [LOCALE_NAMESPACE_NAME.MOLECULES]: MoleculesLocaleNamespace;
  [LOCALE_NAMESPACE_NAME.ASSETS]: AssetsLocaleNamespace;
  [LOCALE_NAMESPACE_NAME.TIMEDATE]: TimedateLocaleNamespace;
};
