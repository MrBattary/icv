import SkillId from '../../../../types/business/skill/SkillId';
import SkillTag from '../../../../types/business/skill/SkillTag';
import TagId from '../../../../types/business/tag/TagId';
import TagGroup, {
  DEFAULT_TAG_GROUP,
} from '../../../../types/business/tag/group/TagGroup';
import Experience from '../../../../types/business/experience/Experience';
import ExperienceGroup from '../../../../types/business/experience/group/ExperienceGroup';
import { DEFAULT_EXPERIENCE_GROUP_ID } from '../../../../types/business/experience/group/ExperienceGroupId';

type SupportMaps = {
  experienceToExperienceIdMap: Map<SkillId, Experience>;
  setOfTagIdToExperienceIdMap: Map<SkillId, Set<TagId>>;
};

const findMaxTagGroupLength = (tagGroups: TagGroup[]): number =>
  tagGroups
    .map((tagGroup) => tagGroup.tags.length)
    .reduce((max, value) => (max < value ? value : max));

const createSupportMaps = (experiences: Experience[]): SupportMaps => {
  const experienceToExperienceIdMap = new Map<SkillId, Experience>();
  const setOfTagIdToExperienceIdMap = new Map<SkillId, Set<TagId>>();

  experiences.forEach((experience) => {
    experienceToExperienceIdMap.set(experience.id, experience);
    setOfTagIdToExperienceIdMap.set(
      experience.id,
      new Set((experience.tags ?? []).map((tag) => tag.id)),
    );
  });

  return {
    experienceToExperienceIdMap,
    setOfTagIdToExperienceIdMap,
  };
};

const initializeEmptyExperienceGroups = (
  tagGroups: TagGroup[],
): ExperienceGroup[] => {
  const experienceGroups: ExperienceGroup[] = [];

  tagGroups.forEach((tagGroup, index) => {
    experienceGroups.push({
      id: index,
      tagGroup,
      experiences: [],
    });
  });

  return experienceGroups;
};

const retrieveExperiencesForProvidedTags = (
  skillTags: SkillTag[],
  experienceToExperienceIdMap: Map<SkillId, Experience>,
  setOfTagIdToExperienceIdMap: Map<SkillId, Set<TagId>>,
): Experience[] => {
  const experienceIdsToRetrieve = new Set<SkillId>();

  skillTags.forEach((skillTag) => {
    setOfTagIdToExperienceIdMap.forEach((tagIdSet, experienceId) => {
      if (tagIdSet.has(skillTag.id)) {
        experienceIdsToRetrieve.add(experienceId);
      }
    });
  });

  const retrievedExperiences: Experience[] = [];
  experienceIdsToRetrieve.forEach((experienceId) => {
    const retrievedExperience = experienceToExperienceIdMap.get(experienceId);
    if (retrievedExperience) {
      retrievedExperiences.push(retrievedExperience);
      experienceToExperienceIdMap.delete(experienceId);
      setOfTagIdToExperienceIdMap.delete(experienceId);
    }
  });

  return retrievedExperiences;
};

const createDefaultExperienceGroup = (
  experienceSourceMap: Map<SkillId, Experience>,
): ExperienceGroup => {
  const ungroupedExperiences: Experience[] = [];

  experienceSourceMap.forEach((experience) => {
    ungroupedExperiences.push(experience);
  });

  return {
    id: DEFAULT_EXPERIENCE_GROUP_ID,
    tagGroup: DEFAULT_TAG_GROUP,
    experiences: ungroupedExperiences,
  };
};

const groupSkillsExperienceByTagGroupsWithDuplicates = (
  tagGroups: TagGroup[],
  experiences: Experience[],
): ExperienceGroup[] => {
  const supportMaps = createSupportMaps(experiences);
  const { experienceToExperienceIdMap } = supportMaps;
  const { setOfTagIdToExperienceIdMap } = supportMaps;

  const experienceGroups = initializeEmptyExperienceGroups(tagGroups);

  const maxTagGroupLength = findMaxTagGroupLength(tagGroups);
  for (
    let currentTagGroupLength = 1;
    currentTagGroupLength <= maxTagGroupLength;
    currentTagGroupLength++
  ) {
    experienceGroups.forEach((experienceGroup) => {
      const currentTagGroup = experienceGroup.tagGroup.tags.slice(
        0,
        currentTagGroupLength,
      );
      experienceGroup.experiences = [
        ...experienceGroup.experiences,
        ...retrieveExperiencesForProvidedTags(
          currentTagGroup,
          experienceToExperienceIdMap,
          setOfTagIdToExperienceIdMap,
        ),
      ];
    });
  }

  experienceGroups.push(
    createDefaultExperienceGroup(experienceToExperienceIdMap),
  );

  return experienceGroups;
};

export default groupSkillsExperienceByTagGroupsWithDuplicates;
