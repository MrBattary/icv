/**
 * Changes the contents of an array by rearranging the elements in a random order.
 *
 * @param array An array to shuffle.
 */
const shuffleArray = <T>(array: T[]) => {
  let currentIndex = array.length;

  while (currentIndex !== 0) {
    const randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }
};

export default shuffleArray;
