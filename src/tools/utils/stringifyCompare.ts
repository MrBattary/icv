const stringifyCompare = <A, B>(a: A, b: B) =>
  JSON.stringify(a).localeCompare(JSON.stringify(b));

export default stringifyCompare;
