const stringifyEquals = <A, B>(a: A, b: B) =>
  JSON.stringify(a) === JSON.stringify(b);

export default stringifyEquals;
