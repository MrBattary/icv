/**
 * Removes undefined keys from the object.
 * If as a result there are no more keys left in the object, makes the object undefined.
 * @param obj Object or undefined.
 */
const undefy = <T extends object>(obj?: T): T | undefined => {
  if (obj) {
    let newObj: T = {} as T;
    (Object.keys(obj) as (keyof T)[]).forEach((key) => {
      if (obj[key] !== undefined) {
        newObj = { ...newObj, [key]: obj[key] };
      }
    });
    if (Object.keys(newObj).length !== 0) {
      return newObj;
    }
  }
  return undefined;
};

export default undefy;
