const deepClone = <T>(value: T): T => structuredClone(value);

export default deepClone;
