import isEqual from 'react-fast-compare';

const deepEquals = <A, B>(a: A, b: B) => isEqual(a, b);

export default deepEquals;
