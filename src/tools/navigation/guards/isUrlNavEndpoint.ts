import UrlNavEndpoint from '../endpoints/UrlNavEndpoint';
import NavEndpoint from '../endpoints/NavEndpoint';

const isUrlNavEndpoint = (endpoint: NavEndpoint): endpoint is UrlNavEndpoint =>
  (endpoint as UrlNavEndpoint).url !== undefined;

export default isUrlNavEndpoint;
