import UrlQuery from '../../web/url/queries/UrlQuery';
import UrlPath from '../../web/url/paths/UrlPath';

type InternalNavEndpoint = {
  path: UrlPath;
  query?: UrlQuery;
};

export default InternalNavEndpoint;
