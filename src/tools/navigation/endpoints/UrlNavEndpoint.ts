import Url from '../../web/url/Url';

type UrlNavEndpoint = {
  url: Url;
};

export default UrlNavEndpoint;
