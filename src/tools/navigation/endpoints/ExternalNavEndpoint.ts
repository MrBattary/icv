import UrlProtocol from '../../web/url/protocols/UrlProtocol';
import UrlDomain from '../../web/url/domains/UrlDomain';
import UrlPath from '../../web/url/paths/UrlPath';
import UrlQuery from '../../web/url/queries/UrlQuery';

type ExternalNavEndpoint = {
  protocol: UrlProtocol;
  domain: UrlDomain;
  path?: UrlPath;
  query?: UrlQuery;
};

export default ExternalNavEndpoint;
