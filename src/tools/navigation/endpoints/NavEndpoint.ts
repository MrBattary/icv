import InternalNavEndpoint from './InternalNavEndpoint';
import ExternalNavEndpoint from './ExternalNavEndpoint';
import UrlNavEndpoint from './UrlNavEndpoint';

type NavEndpoint = InternalNavEndpoint | ExternalNavEndpoint | UrlNavEndpoint;

export default NavEndpoint;
