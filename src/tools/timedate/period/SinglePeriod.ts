import Date from '../date/Date';

type SinglePeriod = {
  start: Date;
  end: Date;
};

export default SinglePeriod;
