import SinglePeriod from '../SinglePeriod';
import MultiPeriod from '../MultiPeriod';

const fromSingleToMultiPeriod = (period: SinglePeriod): MultiPeriod => [period];

export default fromSingleToMultiPeriod;
