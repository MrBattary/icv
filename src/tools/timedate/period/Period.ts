import SinglePeriod from './SinglePeriod';
import MultiPeriod from './MultiPeriod';

type Period = SinglePeriod | MultiPeriod;

export default Period;
