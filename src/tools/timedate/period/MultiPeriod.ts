import SinglePeriod from './SinglePeriod';

type MultiPeriod = SinglePeriod[];

export default MultiPeriod;
