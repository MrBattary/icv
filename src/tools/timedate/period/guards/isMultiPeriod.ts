import Period from '../Period';
import MultiPeriod from '../MultiPeriod';

const isMultiPeriod = (period: Period): period is MultiPeriod =>
  Array.isArray(period as MultiPeriod);

export default isMultiPeriod;
