import SinglePeriod from '../SinglePeriod';
import Period from '../Period';

const isSinglePeriod = (period: Period): period is SinglePeriod =>
  (period as SinglePeriod).start !== undefined &&
  (period as SinglePeriod).end !== undefined;

export default isSinglePeriod;
