export const PRESENT_DATE = 'present';

type PresentDate = typeof PRESENT_DATE;

export default PresentDate;
