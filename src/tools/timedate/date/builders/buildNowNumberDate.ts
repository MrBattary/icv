import NumberDate from '../NumberDate';

import buildNowMoment from './buildNowMoment';
import fromMomentToNumberDate from '../converters/fromMomentToNumberDate';

const buildNowNumberDate = (): NumberDate =>
  fromMomentToNumberDate(buildNowMoment());

export default buildNowNumberDate;
