import moment from 'moment';

const buildNowMoment = (): moment.Moment => moment();

export default buildNowMoment;
