import NumberDate from './NumberDate';
import PresentDate from './PresentDate';

type Date = NumberDate | PresentDate;

export default Date;
