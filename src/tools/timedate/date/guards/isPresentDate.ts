import Date from '../Date';
import PresentDate, { PRESENT_DATE } from '../PresentDate';

const isPresentDate = (date: Date): date is PresentDate =>
  (date as PresentDate) === PRESENT_DATE;

export default isPresentDate;
