export const CAPITAL_PRESENT_DATE_FORMAT = 'CAPITAL';
export const LOWERCASE_PRESENT_DATE_FORMAT = 'LOWERCASE';

type PresentDateFormat =
  | typeof CAPITAL_PRESENT_DATE_FORMAT
  | typeof LOWERCASE_PRESENT_DATE_FORMAT;

export default PresentDateFormat;
