import FixedDateFormat from './FixedDateFormat';
import LocaleDateFormat from './LocaleDateFormat';

type DateFormat = FixedDateFormat | LocaleDateFormat;

export default DateFormat;
