export const MM_SLASH_DD_SLASH_YYYY_FIXED_DATE_FORMAT = 'MM/DD/YYYY';
export const DD_SLASH_MM_SLASH_YYYY_FIXED_DATE_FORMAT = 'DD/MM/YYYY';

type CustomFixedDateFormat = string;

type FixedDateFormat =
  | CustomFixedDateFormat
  | typeof MM_SLASH_DD_SLASH_YYYY_FIXED_DATE_FORMAT
  | typeof DD_SLASH_MM_SLASH_YYYY_FIXED_DATE_FORMAT;

export default FixedDateFormat;
