export const L_LOCALE_DATE_FORMAT = 'L';
export const ll_LOCALE_DATE_FORMAT = 'll';

// moment.js multiple locale support formats
type LocaleDateFormat =
  | typeof L_LOCALE_DATE_FORMAT
  | typeof ll_LOCALE_DATE_FORMAT;

export default LocaleDateFormat;
