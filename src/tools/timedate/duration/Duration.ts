type Duration = {
  years: number;
  months: number;
  days: number;
};

export default Duration;
