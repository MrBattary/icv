import UrlQueryValues from './UrlQueryValues';

type UrlQueryObject = Record<string, UrlQueryValues>;

export default UrlQueryObject;
