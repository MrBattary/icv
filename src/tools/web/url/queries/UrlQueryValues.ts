type UrlQueryValues = number | string | boolean;

export default UrlQueryValues;
