export const HTTP = 'http://';
export const HTTPS = 'https://';

type UrlProtocol = typeof HTTP | typeof HTTPS;

export default UrlProtocol;
