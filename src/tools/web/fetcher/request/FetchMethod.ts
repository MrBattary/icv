export const FETCH_METHOD_GET = 'GET';

type FetchMethod = typeof FETCH_METHOD_GET;

export default FetchMethod;
