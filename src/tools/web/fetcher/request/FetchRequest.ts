import FetchHeaders from './FetchHeaders';
import Url from '../../url/Url';

type FetchRequest = {
  url: Url;
  headers: FetchHeaders;
};

export default FetchRequest;
