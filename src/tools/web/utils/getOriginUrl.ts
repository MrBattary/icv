import Url from '../url/Url';

const getOriginUrl = (): Url => document.location.origin;

export default getOriginUrl;
