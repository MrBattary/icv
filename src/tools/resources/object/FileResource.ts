import ResourceObjectPrototype from './prototype/ResourceObjectPrototype';
import DirectoryResource from './DirectoryResource';
import FileResourceExtension from './fields/extension/FileResourceExtension';

type FileResource = ResourceObjectPrototype & {
  type: FileResourceExtension;
  parent: DirectoryResource;
};

export default FileResource;
