import ResourceObjectPrototype from './prototype/ResourceObjectPrototype';
import DirectoryResourceExtension from './fields/extension/DirectoryResourceExtension';

type DirectoryResource = ResourceObjectPrototype & {
  type: DirectoryResourceExtension;
};

export default DirectoryResource;
