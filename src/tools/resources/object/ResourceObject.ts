import FileResource from './FileResource';
import DirectoryResource from './DirectoryResource';

type ResourceObject = FileResource | DirectoryResource;

export default ResourceObject;
