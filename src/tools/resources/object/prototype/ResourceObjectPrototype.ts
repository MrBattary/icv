import ResourceExtension from '../fields/extension/ResourceExtension';
import ResourceName from '../fields/ResourceName';
import DirectoryResource from '../DirectoryResource';
import ResourceDescription from '../fields/ResourceDescription';

type ResourceObjectPrototype = {
  type: ResourceExtension;
  name?: ResourceName;
  description?: ResourceDescription;
  parent?: DirectoryResource;
};

export default ResourceObjectPrototype;
