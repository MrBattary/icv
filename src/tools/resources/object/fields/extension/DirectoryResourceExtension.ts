export const DIRECTORY_RESOURCE = 'directory';

type DirectoryResourceExtension = typeof DIRECTORY_RESOURCE;

export default DirectoryResourceExtension;
