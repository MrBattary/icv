import FileResourceExtension from './FileResourceExtension';
import DirectoryResourceExtension from './DirectoryResourceExtension';

type ResourceExtension = FileResourceExtension | DirectoryResourceExtension;

export default ResourceExtension;
