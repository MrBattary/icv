import Url from '../../web/url/Url';
import getOriginUrl from '../../web/utils/getOriginUrl';
import ResourcePath from '../object/fields/ResourcePath';

const buildResourceUrl = (resourcePath: ResourcePath): Url =>
  getOriginUrl().concat(resourcePath.path);

export default buildResourceUrl;
