import Resource from './Resource';

type LoadableResource<T extends Resource> = T | null;

export default LoadableResource;
