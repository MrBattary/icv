import ActivityResource from '../ActivityResource';
import ProjectActivityResource from '../variants/ProjectActivityResource';

const isProjectActivityResource = (
  activityResource: ActivityResource,
): activityResource is ProjectActivityResource =>
  (activityResource as ProjectActivityResource).projectId !== undefined;

export default isProjectActivityResource;
