import ActivityResource from '../ActivityResource';
import ProviderActivityResource from '../variants/ProviderActivityResource';

const isProviderActivityResource = (
  activityResource: ActivityResource,
): activityResource is ProviderActivityResource =>
  (activityResource as ProviderActivityResource).providerId !== undefined;

export default isProviderActivityResource;
