import TextCommonResource from '../../../common/TextCommonResource';
import PeriodCommonResource from '../../../common/PeriodCommonResource';

type ActivityResourceType =
  | 'personal'
  | 'commercial'
  | 'startup'
  | 'internship'
  | 'education';

type ActivityResourcePrototype = {
  id: TextCommonResource;
  heirId?: TextCommonResource;
  type: ActivityResourceType;
  period: PeriodCommonResource;
  positionId: TextCommonResource;
  subactivitiesIds: TextCommonResource[];
};

export default ActivityResourcePrototype;
