import ProjectActivityResource from './variants/ProjectActivityResource';
import ProjectProviderActivityResource from './variants/ProjectProviderActivityResource';
import ProviderActivityResource from './variants/ProviderActivityResource';

type ActivityResource =
  | ProjectActivityResource
  | ProjectProviderActivityResource
  | ProviderActivityResource;

export default ActivityResource;
