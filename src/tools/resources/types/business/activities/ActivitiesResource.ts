import ActivityResource from './ActivityResource';

type ActivitiesResource = ActivityResource[];

export default ActivitiesResource;
