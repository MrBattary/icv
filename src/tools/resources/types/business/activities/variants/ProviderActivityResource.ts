import ActivityResourcePrototype from '../prototype/ActivityResourcePrototype';
import TextCommonResource from '../../../common/TextCommonResource';

type ProviderActivityResource = ActivityResourcePrototype & {
  providerId: TextCommonResource;
};

export default ProviderActivityResource;
