import ActivityResourcePrototype from '../prototype/ActivityResourcePrototype';
import TextCommonResource from '../../../common/TextCommonResource';

type ProjectActivityResource = ActivityResourcePrototype & {
  projectId: TextCommonResource;
};

export default ProjectActivityResource;
