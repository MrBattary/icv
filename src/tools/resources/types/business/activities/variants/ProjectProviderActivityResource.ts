import ActivityResourcePrototype from '../prototype/ActivityResourcePrototype';
import TextCommonResource from '../../../common/TextCommonResource';

type ProjectProviderActivityResource = ActivityResourcePrototype & {
  projectId: TextCommonResource;
  providerId: TextCommonResource;
};

export default ProjectProviderActivityResource;
