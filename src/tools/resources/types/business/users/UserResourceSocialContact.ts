import TextCommonResource from '../../common/TextCommonResource';

type UserSocialContactTypes = 'email' | 'telegram';

type UserResourceSocialContact = {
  type: UserSocialContactTypes;
  value: TextCommonResource;
};

export default UserResourceSocialContact;
