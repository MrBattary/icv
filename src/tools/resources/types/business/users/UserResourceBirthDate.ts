type YearBirthDateResource = {
  year: number;
};

type YearMonthBirthDateResource = {
  year: number;
  month: number;
};

type YearMonthDayBirthDateResource = {
  year: number;
  month: number;
  day: number;
};

type UserResourceBirthDate =
  | YearBirthDateResource
  | YearMonthBirthDateResource
  | YearMonthDayBirthDateResource;

export default UserResourceBirthDate;
