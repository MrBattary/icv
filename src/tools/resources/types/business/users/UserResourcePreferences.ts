import TextCommonResource from '../../common/TextCommonResource';

type ContactsUserPreferenceTypes = 'active' | 'regular' | 'limited';

type TagGroupUserPreference = {
  name?: TextCommonResource;
  tagIds: TextCommonResource[];
};

type UserResourcePreferences = {
  contacts?: ContactsUserPreferenceTypes;
  tagGroups?: TagGroupUserPreference[];
};

export default UserResourcePreferences;
