import TextCommonResource from '../../common/TextCommonResource';
import ImageCommonResource from '../../common/ImageCommonResource';
import NumberCommonResource from '../../common/NumberCommonResource';
import LocationCommonResource from '../../common/LocationCommonResource';
import UserResourceBirthDate from './UserResourceBirthDate';
import UserResourceSocialLink from './UserResourceSocialLink';
import UserResourceSocialContact from './UserResourceSocialContact';
import UserResourcePreferences from './UserResourcePreferences';

type UserResource = {
  id: TextCommonResource;
  avatar?: ImageCommonResource;
  firstName: TextCommonResource;
  middleName?: TextCommonResource;
  lastName?: TextCommonResource;
  age?: NumberCommonResource;
  birthdate?: UserResourceBirthDate;
  currentPositionId?: TextCommonResource;
  location?: LocationCommonResource;
  social?: {
    contacts: UserResourceSocialContact[];
    links: UserResourceSocialLink[];
  };
  preferences?: UserResourcePreferences;
};

export default UserResource;
