import TextCommonResource from '../../common/TextCommonResource';

type UserSocialLinkTypes = 'gitlab' | 'github' | 'linkedin';

type IntegratedUserSocialResourceLink = {
  type: UserSocialLinkTypes;
  path: TextCommonResource;
};

type CommonUserSocialResourceLink = {
  type: 'common';
  url: TextCommonResource;
};

type UserResourceSocialLink =
  | IntegratedUserSocialResourceLink
  | CommonUserSocialResourceLink;

export default UserResourceSocialLink;
