import TagResource from './TagResource';

type TagsResource = TagResource[];

export default TagsResource;
