import TextCommonResource from '../../common/TextCommonResource';

type TagResource = {
  id: TextCommonResource;
  name: TextCommonResource;
};

export default TagResource;
