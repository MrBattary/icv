import TextCommonResource from '../../common/TextCommonResource';
import PeriodCommonResource from '../../common/PeriodCommonResource';

type SubactivityResource = {
  id: TextCommonResource;
  period: PeriodCommonResource;
  description?: TextCommonResource;
  skillsIds: TextCommonResource[];
};

export default SubactivityResource;
