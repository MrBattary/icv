import SubactivityResource from './SubactivityResource';

type SubactivitiesResource = SubactivityResource[];

export default SubactivitiesResource;
