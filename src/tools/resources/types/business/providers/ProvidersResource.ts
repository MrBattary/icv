import ProviderResource from './ProviderResource';

type ProvidersResource = ProviderResource[];

export default ProvidersResource;
