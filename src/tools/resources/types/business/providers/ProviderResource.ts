import TextCommonResource from '../../common/TextCommonResource';
import ImageCommonResource from '../../common/ImageCommonResource';
import LinkCommonResource from '../../common/LinkCommonResource';

type ProviderResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  avatar?: ImageCommonResource;
  link?: LinkCommonResource;
};

export default ProviderResource;
