import PositionResource from '../PositionResource';

type ProcessedPositionResource = PositionResource &
  Required<Pick<PositionResource, 'color'>>;

export default ProcessedPositionResource;
