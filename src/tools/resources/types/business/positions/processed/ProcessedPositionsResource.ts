import ProcessedPositionResource from './ProcessedPositionResource';

type ProcessedPositionsResource = ProcessedPositionResource[];

export default ProcessedPositionsResource;
