import PositionResource from './PositionResource';

type PositionsResource = PositionResource[];

export default PositionsResource;
