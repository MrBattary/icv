import TextCommonResource from '../../common/TextCommonResource';
import HexColorCommonResource from '../../common/HexColorCommonResource';

type PositionResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  color?: HexColorCommonResource;
  specializationId: TextCommonResource;
};

export default PositionResource;
