import TextCommonResource from '../../common/TextCommonResource';

type LanguageBundleLocaleCode = TextCommonResource;

type LanguageBundlesResource = {
  mainBundle: LanguageBundleLocaleCode;
  bundleAddons: {
    activities: LanguageBundleLocaleCode[];
    cv: LanguageBundleLocaleCode[];
    positions: LanguageBundleLocaleCode[];
    projects: LanguageBundleLocaleCode[];
    providers: LanguageBundleLocaleCode[];
    skills: LanguageBundleLocaleCode[];
    subactivities: LanguageBundleLocaleCode[];
    user: LanguageBundleLocaleCode[];
    tags: LanguageBundleLocaleCode[];
    specializations: LanguageBundleLocaleCode[];
  };
};

export default LanguageBundlesResource;
