import TextCommonResource from '../../common/TextCommonResource';

type CvResource = {
  userId: TextCommonResource;
  activitiesIds: TextCommonResource[];
};

export default CvResource;
