import ProcessedSkillResource from './ProcessedSkillResource';

type ProcessedSkillsResource = ProcessedSkillResource[];

export default ProcessedSkillsResource;
