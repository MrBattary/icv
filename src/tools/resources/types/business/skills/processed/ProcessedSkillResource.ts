import SkillResource from '../SkillResource';

type ProcessedSkillResource = SkillResource;

export default ProcessedSkillResource;
