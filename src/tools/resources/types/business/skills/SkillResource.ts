import TextCommonResource from '../../common/TextCommonResource';
import ImageCommonResource from '../../common/ImageCommonResource';
import HexColorCommonResource from '../../common/HexColorCommonResource';

type SkillResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  avatar?: ImageCommonResource;
  color?: HexColorCommonResource;
  tagIds?: TextCommonResource[];
};

export default SkillResource;
