import SkillResource from './SkillResource';

type SkillsResource = SkillResource[];

export default SkillsResource;
