import ProjectResource from './ProjectResource';

type ProjectsResource = ProjectResource[];

export default ProjectsResource;
