import TextCommonResource from '../../common/TextCommonResource';

type ProjectResourceLinkTypes = 'gitlab' | 'github';

type IntegratedProjectResourceLink = {
  type: ProjectResourceLinkTypes;
  path: TextCommonResource;
};

type CommonProjectResourceLink = {
  type: 'common';
  url: TextCommonResource;
};

type ProjectResourceLink =
  | IntegratedProjectResourceLink
  | CommonProjectResourceLink;

export default ProjectResourceLink;
