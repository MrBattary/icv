import TextCommonResource from '../../common/TextCommonResource';
import ImageCommonResource from '../../common/ImageCommonResource';
import ProjectResourceLink from './ProjectResourceLink';

type ProjectResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  avatar?: ImageCommonResource;
  links?: ProjectResourceLink[];
};

export default ProjectResource;
