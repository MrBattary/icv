import TextCommonResource from '../../../common/TextCommonResource';
import HexColorCommonResource from '../../../common/HexColorCommonResource';

type ProcessedSpecializationResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  color: HexColorCommonResource;
};

export default ProcessedSpecializationResource;
