import ProcessedSpecializationResource from './ProcessedSpecializationResource';

type ProcessedSpecializationsResource = ProcessedSpecializationResource[];

export default ProcessedSpecializationsResource;
