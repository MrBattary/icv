import SpecializationResource from './SpecializationResource';

type SpecializationsResource = SpecializationResource[];

export default SpecializationsResource;
