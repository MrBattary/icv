import TextCommonResource from '../../common/TextCommonResource';
import HexColorCommonResource from '../../common/HexColorCommonResource';

type SpecializationResource = {
  id: TextCommonResource;
  name: TextCommonResource;
  color?: HexColorCommonResource;
};

export default SpecializationResource;
