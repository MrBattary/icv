import TextCommonResource from '../../common/TextCommonResource';

type VersionResource = TextCommonResource;

export default VersionResource;
