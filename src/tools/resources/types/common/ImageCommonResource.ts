type ImageCommonResourceShape = 'circular' | 'rounded' | 'rectangular';

type ThemedImageCommonResource = {
  src: {
    light: string;
    dark: string;
  };
  alt?: string;
  shape?: ImageCommonResourceShape;
};

type NonThemedImageCommonResource = {
  src: string;
  alt?: string;
  shape?: ImageCommonResourceShape;
};

type ImageCommonResource =
  | NonThemedImageCommonResource
  | ThemedImageCommonResource;

export default ImageCommonResource;
