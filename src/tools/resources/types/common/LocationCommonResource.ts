import TextCommonResource from './TextCommonResource';
import NumberCommonResource from './NumberCommonResource';

type GeolocationCommonResource =
  | { prompt: TextCommonResource }
  | { latitude: NumberCommonResource; longitude: NumberCommonResource }
  | {
      latitude: NumberCommonResource;
      longitude: NumberCommonResource;
      placeId: TextCommonResource;
    };

type LocationCommonResource = {
  name: TextCommonResource;
  mapping?: GeolocationCommonResource;
};

export default LocationCommonResource;
