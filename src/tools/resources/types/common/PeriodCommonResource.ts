import DateCommonResource from './DateCommonResource';

type PeriodCommonResource = {
  start: DateCommonResource;
  end: DateCommonResource;
};

export default PeriodCommonResource;
