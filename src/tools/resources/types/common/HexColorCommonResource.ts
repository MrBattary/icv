type NonThemedHexColorCommonResource = string;

type ThemedHexColorCommonResource = {
  light: string;
  dark: string;
};

type HexColorCommonResource =
  | NonThemedHexColorCommonResource
  | ThemedHexColorCommonResource;

export default HexColorCommonResource;
