type DateCommonResource =
  | {
      year: number;
      month: number;
      day: number;
    }
  | 'present';

export default DateCommonResource;
