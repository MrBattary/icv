import CvResource from './business/cv/CvResource';
import VersionResource from './business/version/VersionResource';
import UserResource from './business/users/UserResource';
import ActivitiesResource from './business/activities/ActivitiesResource';
import PositionsResource from './business/positions/PositionsResource';
import ProjectsResource from './business/projects/ProjectsResource';
import ProvidersResource from './business/providers/ProvidersResource';
import SkillsResource from './business/skills/SkillsResource';
import SubactivitiesResource from './business/subactivities/SubactivitiesResource';
import LanguageBundlesResource from './business/languageBundles/LanguageBundlesResource';

type Resource =
  | VersionResource
  | LanguageBundlesResource
  | CvResource
  | UserResource
  | ActivitiesResource
  | SubactivitiesResource
  | PositionsResource
  | ProjectsResource
  | ProvidersResource
  | SkillsResource;

export default Resource;
