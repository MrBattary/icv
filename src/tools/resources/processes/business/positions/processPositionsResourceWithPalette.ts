import PositionsResource from '../../../types/business/positions/PositionsResource';
import ProcessedPositionsResource from '../../../types/business/positions/processed/ProcessedPositionsResource';
import SpecializationsResource from '../../../types/business/specializations/SpecializationsResource';
import ThemedHexPaletteCommon from '../../../../../types/common/media/palette/hex/variants/ThemedHexPaletteCommon';

import ResourceProcessResult from '../../types/result/ResourceProcessResult';
import { FAILURE_RESOURCE_PROCESS_RESULT } from '../../types/result/variants/FailureResourceProcessResult';

import processResourceWithPalette from '../../common/processResourceWithPalette';

const processPositionsResourceWithPalette = (
  positionsResource: PositionsResource,
  specializationsResource: SpecializationsResource | null,
  palette: ThemedHexPaletteCommon,
): ResourceProcessResult<ProcessedPositionsResource> => {
  if (specializationsResource) {
    let numberOfUsedColorsBySpecializations = 0;
    specializationsResource.forEach((specialization) => {
      if (specialization.color) {
        numberOfUsedColorsBySpecializations += 1;
      }
    });

    let numberOfToBeUsedColorsByPositions = 0;
    positionsResource.forEach((position) => {
      if (position.color) {
        numberOfToBeUsedColorsByPositions += 1;
      }
    });

    // The entire palette is used by default in case if there are not enough colors in the palette for the positions
    const positionsPalette: ThemedHexPaletteCommon = {
      light: palette.light,
      dark: palette.dark,
    };

    const numberOfUnusedLightPaletteColors =
      palette.light.length - numberOfUsedColorsBySpecializations;
    const numberOfUnusedDarkPaletteColors =
      palette.dark.length - numberOfUsedColorsBySpecializations;

    // If there are enough colors in the palette for the positions, then the used colors will be removed
    if (numberOfUnusedLightPaletteColors >= numberOfToBeUsedColorsByPositions) {
      positionsPalette.light = positionsPalette.light.slice(
        numberOfUsedColorsBySpecializations,
      );
    }
    if (numberOfUnusedDarkPaletteColors >= numberOfToBeUsedColorsByPositions) {
      positionsPalette.dark = positionsPalette.dark.slice(
        numberOfUsedColorsBySpecializations,
      );
    }

    return {
      success: true,
      resource: processResourceWithPalette(positionsResource, positionsPalette),
    };
  }

  return FAILURE_RESOURCE_PROCESS_RESULT;
};

export default processPositionsResourceWithPalette;
