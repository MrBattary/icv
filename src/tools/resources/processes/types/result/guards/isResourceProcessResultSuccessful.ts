import Resource from '../../../../types/Resource';
import ResourceProcessResult from '../ResourceProcessResult';
import SuccessfulResourceProcessResult from '../variants/SuccessfulResourceProcessResult';

const isResourceProcessResultSuccessful = <T extends Resource>(
  result: ResourceProcessResult<T>,
): result is SuccessfulResourceProcessResult<T> => result.success;

export default isResourceProcessResultSuccessful;
