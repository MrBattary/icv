import Resource from '../../../types/Resource';
import FailureResourceProcessResult from './variants/FailureResourceProcessResult';
import SuccessfulResourceProcessResult from './variants/SuccessfulResourceProcessResult';

type ResourceProcessResult<T extends Resource> =
  | FailureResourceProcessResult
  | SuccessfulResourceProcessResult<T>;

export default ResourceProcessResult;
