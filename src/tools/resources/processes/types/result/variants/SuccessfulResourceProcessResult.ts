import Resource from '../../../../types/Resource';
import FailureResourceProcessResult from './FailureResourceProcessResult';

type SuccessfulResourceProcessResult<T extends Resource> =
  FailureResourceProcessResult & {
    resource: T;
  };

export default SuccessfulResourceProcessResult;
