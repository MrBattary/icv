export const FAILURE_RESOURCE_PROCESS_RESULT: FailureResourceProcessResult = {
  success: false,
};

type FailureResourceProcessResult = {
  success: boolean;
};

export default FailureResourceProcessResult;
