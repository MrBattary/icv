import ConstructStage from './ConstructStage';

type ConstructFlow<T, R> = ConstructStage<T, R>[];

export default ConstructFlow;
