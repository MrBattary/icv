import ConstructFlow from './ConstructFlow';

type Constructed<F extends ConstructFlow<unknown, unknown>, V> = {
  flow: F;
  value: V;
};

export default Constructed;
