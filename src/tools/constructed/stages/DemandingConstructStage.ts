type DemandingConstructStage<T, R> = {
  name: T;
  require: R[];
};

export default DemandingConstructStage;
