import ConstructStage from '../ConstructStage';
import ConstructFlow from '../ConstructFlow';

const isStageExistsInFlow = <T, R extends T>(
  stage: ConstructStage<T, R>,
  flow: ConstructFlow<T, R>,
): boolean => flow.some((stageInFlow) => stage.name === stageInFlow.name);
export default isStageExistsInFlow;
