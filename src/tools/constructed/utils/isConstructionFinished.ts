import ConstructFlow from '../ConstructFlow';
import Constructed from '../Constructed';

const isConstructionFinished = <F extends ConstructFlow<unknown, unknown>, V>(
  constructed: Constructed<F, V>,
) => constructed.flow.length === 0;

export default isConstructionFinished;
