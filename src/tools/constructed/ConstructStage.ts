import DemandingConstructStage from './stages/DemandingConstructStage';
import UndemandingConstructStage from './stages/UndemandingConstructStage';

type ConstructStage<T, R> =
  | DemandingConstructStage<T, R>
  | UndemandingConstructStage<T>;

export default ConstructStage;
