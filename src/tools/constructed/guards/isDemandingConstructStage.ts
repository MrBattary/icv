import ConstructStage from '../ConstructStage';
import DemandingConstructStage from '../stages/DemandingConstructStage';

const isDemandingConstructStage = <T, R>(
  stage: ConstructStage<T, R>,
): stage is DemandingConstructStage<T, R> =>
  (stage as DemandingConstructStage<T, R>).require !== undefined;

export default isDemandingConstructStage;
