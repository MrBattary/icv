import GoogleMapsSearch from './google/search/GoogleMapsSearch';

type GeolocationMapping = GoogleMapsSearch;

export default GeolocationMapping;
