import ApiGoogleMapsSearchQuery from './ApiGoogleMapsSearchQuery';

const apiGoogleMapsSearchQueryValue: ApiGoogleMapsSearchQuery = {
  api: 1,
};

export default apiGoogleMapsSearchQueryValue;
