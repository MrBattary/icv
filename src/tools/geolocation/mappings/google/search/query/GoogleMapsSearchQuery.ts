import PromptGoogleMapsSearchQuery from './PromptGoogleMapsSearchQuery';
import CoordinatesGoogleMapsSearchQuery from './CoordinatesGoogleMapsSearchQuery';
import PlaceGoogleMapsSearchQuery from './PlaceGoogleMapsSearchQuery';

type GoogleMapsSearchQuery =
  | PromptGoogleMapsSearchQuery
  | CoordinatesGoogleMapsSearchQuery
  | PlaceGoogleMapsSearchQuery;

export default GoogleMapsSearchQuery;
