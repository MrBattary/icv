import ApiGoogleMapsSearchQuery from './api/ApiGoogleMapsSearchQuery';

type CoordinatesGoogleMapsSearchQuery = ApiGoogleMapsSearchQuery & {
  query: string;
};

export default CoordinatesGoogleMapsSearchQuery;
