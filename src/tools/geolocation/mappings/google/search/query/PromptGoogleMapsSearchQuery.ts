import ApiGoogleMapsSearchQuery from './api/ApiGoogleMapsSearchQuery';

type PromptGoogleMapsSearchQuery = ApiGoogleMapsSearchQuery & {
  query: string;
};

export default PromptGoogleMapsSearchQuery;
