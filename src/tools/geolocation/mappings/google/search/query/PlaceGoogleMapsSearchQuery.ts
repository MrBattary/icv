import CoordinatesGoogleMapsSearchQuery from './CoordinatesGoogleMapsSearchQuery';

type PlaceGoogleMapsSearchQuery = CoordinatesGoogleMapsSearchQuery & {
  query_place_id: string;
};

export default PlaceGoogleMapsSearchQuery;
