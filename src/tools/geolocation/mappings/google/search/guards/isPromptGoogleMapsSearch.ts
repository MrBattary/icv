import GoogleMapsSearch from '../GoogleMapsSearch';
import PromptGoogleMapsSearch from '../PromptGoogleMapsSearch';

const isPromptGoogleMapsSearch = (
  googleMapsSearch: GoogleMapsSearch,
): googleMapsSearch is PromptGoogleMapsSearch =>
  (googleMapsSearch as PromptGoogleMapsSearch).prompt !== undefined;

export default isPromptGoogleMapsSearch;
