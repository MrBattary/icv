import CoordinatesGoogleMapsSearch from './CoordinatesGoogleMapsSearch';

type PlaceGoogleMapsSearch = CoordinatesGoogleMapsSearch & {
  placeId: string;
};

export default PlaceGoogleMapsSearch;
