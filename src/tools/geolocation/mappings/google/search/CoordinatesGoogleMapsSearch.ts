type CoordinatesGoogleMapsSearch = {
  latitude: number;
  longitude: number;
};

export default CoordinatesGoogleMapsSearch;
