type PromptGoogleMapsSearch = {
  prompt: string;
};

export default PromptGoogleMapsSearch;
