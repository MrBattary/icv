import CoordinatesGoogleMapsSearch from './CoordinatesGoogleMapsSearch';
import PlaceGoogleMapsSearch from './PlaceGoogleMapsSearch';
import PromptGoogleMapsSearch from './PromptGoogleMapsSearch';

type GoogleMapsSearch =
  | PlaceGoogleMapsSearch
  | CoordinatesGoogleMapsSearch
  | PromptGoogleMapsSearch;

export default GoogleMapsSearch;
