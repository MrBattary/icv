import GeolocationMapping from './mappings/GeolocationMapping';

type GeolocationPlaceName = string;

type GeolocationPlace = {
  name: GeolocationPlaceName;
  mapping?: GeolocationMapping;
};

export default GeolocationPlace;
