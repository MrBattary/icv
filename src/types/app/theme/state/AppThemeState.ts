import AppThemeMode from '../mode/AppThemeMode';
import AppPalette from '../palette/AppPalette';

type AppThemeState = {
  mode: AppThemeMode;
  // State to match the produced palette object
  palette: { palette: AppPalette };
};

export default AppThemeState;
