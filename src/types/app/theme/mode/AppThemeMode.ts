import { PaletteMode } from '@mui/material';

export const LIGHT_THEME_MODE: PaletteMode = 'light';
export const DARK_THEME_MODE: PaletteMode = 'dark';

type AppThemeMode = typeof LIGHT_THEME_MODE | typeof DARK_THEME_MODE;

export default AppThemeMode;
