import { Theme } from '@mui/material';
import AppPalette from './palette/AppPalette';

type AppTheme = Theme & {
  palette: AppPalette;
};

export default AppTheme;
