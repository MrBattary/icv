import { Palette } from '@mui/material/styles/createPalette';

import NonThemedHexColorCommon from '../../../common/media/color/hex/variants/NonThemedHexColorCommon';

type AppPalette = Partial<Palette> & {
  primary: {
    ghost?: NonThemedHexColorCommon;
    phantom?: NonThemedHexColorCommon;
  };
  background: { board?: NonThemedHexColorCommon };
  error: { phantom?: NonThemedHexColorCommon };
  chart?: NonThemedHexColorCommon[];
};

export default AppPalette;
