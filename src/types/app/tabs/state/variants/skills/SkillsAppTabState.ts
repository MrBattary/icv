import AppTabState from '../../AppTabState';
import SkillsAppTabListCards from '../../fields/skills/cards/SkillsAppTabListCards';

type SkillsAppTabState = AppTabState & {
  listCards: SkillsAppTabListCards;
};

export default SkillsAppTabState;
