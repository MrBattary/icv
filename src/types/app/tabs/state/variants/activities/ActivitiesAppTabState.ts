import AppTabState from '../../AppTabState';
import ActivitiesAppTabExpandedCards from '../../fields/activities/cards/ActivitiesAppTabExpandedCards';
import ActivitiesAppTabExpandedDescriptionCards from '../../fields/activities/cards/ActivitiesAppTabExpandedDescriptionCards';
import ActivitiesAppTabExpandedSkillsCards from '../../fields/activities/cards/ActivitiesAppTabExpandedSkillsCards';

type ActivitiesAppTabState = AppTabState & {
  expandedCards: ActivitiesAppTabExpandedCards;
  expandedDescriptionCards: ActivitiesAppTabExpandedDescriptionCards;
  expandedSkillsCards: ActivitiesAppTabExpandedSkillsCards;
};

export default ActivitiesAppTabState;
