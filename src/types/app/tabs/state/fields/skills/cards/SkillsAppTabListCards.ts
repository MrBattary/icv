import ExperienceGroupId from '../../../../../../business/experience/group/ExperienceGroupId';

export const DEFAULT_SKILLS_APP_TAB_LIST_CARDS: SkillsAppTabListCards = [];

type SkillsAppTabListCards = ExperienceGroupId[];

export default SkillsAppTabListCards;
