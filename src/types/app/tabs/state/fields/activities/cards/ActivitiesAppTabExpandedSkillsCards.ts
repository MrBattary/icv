import ActivityId from '../../../../../../business/activity/ActivityId';

export const DEFAULT_ACTIVITIES_APP_TAB_EXPANDED_SKILLS_CARDS: ActivitiesAppTabExpandedSkillsCards =
  [];

type ActivitiesAppTabExpandedSkillsCards = ActivityId[];

export default ActivitiesAppTabExpandedSkillsCards;
