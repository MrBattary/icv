import ActivityId from '../../../../../../business/activity/ActivityId';

export const DEFAULT_ACTIVITIES_APP_TAB_EXPANDED_CARDS: ActivitiesAppTabExpandedCards =
  [];

type ActivitiesAppTabExpandedCards = ActivityId[];

export default ActivitiesAppTabExpandedCards;
