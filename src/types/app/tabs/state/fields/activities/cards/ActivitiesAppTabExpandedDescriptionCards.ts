import ActivityId from '../../../../../../business/activity/ActivityId';

export const DEFAULT_ACTIVITIES_APP_TAB_EXPANDED_DESCRIPTION_CARDS: ActivitiesAppTabExpandedDescriptionCards =
  [];

type ActivitiesAppTabExpandedDescriptionCards = ActivityId[];

export default ActivitiesAppTabExpandedDescriptionCards;
