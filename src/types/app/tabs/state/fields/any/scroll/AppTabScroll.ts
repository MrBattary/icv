export const DEFAULT_TAB_SCROLL: AppTabScroll = 0;

type AppTabScroll = number;

export default AppTabScroll;
