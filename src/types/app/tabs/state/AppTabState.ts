import AppTabScroll from './fields/any/scroll/AppTabScroll';

type AppTabState = {
  scroll: AppTabScroll;
};

export default AppTabState;
