import ActivitiesAppTabState from './variants/activities/ActivitiesAppTabState';
import PositionsAppTabState from './variants/positions/PositionsAppTabState';
import SkillsAppTabState from './variants/skills/SkillsAppTabState';
import {
  ACTIVITIES_APP_TAB_KEY,
  POSITIONS_APP_TAB_KEY,
  SKILLS_APP_TAB_KEY,
} from '../key/AppTabKey';

type AppTabsState = {
  [ACTIVITIES_APP_TAB_KEY]: ActivitiesAppTabState;
  [POSITIONS_APP_TAB_KEY]: PositionsAppTabState;
  [SKILLS_APP_TAB_KEY]: SkillsAppTabState;
};

export default AppTabsState;
