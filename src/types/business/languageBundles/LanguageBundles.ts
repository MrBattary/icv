import I18nLanguage from '../../../tools/internationalization/i18n/types/I18nLanguage';

type LanguageBundles = {
  mainBundle: I18nLanguage;
  bundleAddons: {
    activities: I18nLanguage[];
    cv: I18nLanguage[];
    positions: I18nLanguage[];
    projects: I18nLanguage[];
    providers: I18nLanguage[];
    skills: I18nLanguage[];
    subactivities: I18nLanguage[];
    user: I18nLanguage[];
    tags: I18nLanguage[];
    specializations: I18nLanguage[];
  };
};

export default LanguageBundles;
