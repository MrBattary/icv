import SinglePeriod from '../../../tools/timedate/period/SinglePeriod';

type SubactivityPeriod = SinglePeriod;

export default SubactivityPeriod;
