import TagId from './TagId';
import TagName from './TagName';

type Tag = {
  id: TagId;
  name: TagName;
};

export default Tag;
