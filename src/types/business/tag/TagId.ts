import TextCommon from '../../common/TextCommon';

export const DEFAULT_TAG_ID: TagId = 'DEFAULT';

type TagId = TextCommon;

export default TagId;
