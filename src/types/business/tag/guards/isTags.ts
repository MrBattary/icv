import Tag from '../Tag';
import isTag from './isTag';
import isArray from '../../../common/guards/isArray';

const isTags = (tags: unknown): tags is Tag[] =>
  isArray(tags) && tags.length > 0 && tags.every((tag) => isTag(tag));

export default isTags;
