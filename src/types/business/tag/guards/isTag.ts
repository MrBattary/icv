import Tag from '../Tag';
import isObject from '../../../common/guards/isObject';

const isTag = (tag: unknown): tag is Tag =>
  isObject(tag) &&
  (tag as Tag).id !== undefined &&
  (tag as Tag).name !== undefined;

export default isTag;
