import HexColorCommon from '../../common/media/color/hex/HexColorCommon';

type PositionColor = HexColorCommon;

export default PositionColor;
