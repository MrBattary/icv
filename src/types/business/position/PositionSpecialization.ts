import Specialization from '../specialization/Specialization';

type PositionSpecialization = Specialization;

export default PositionSpecialization;
