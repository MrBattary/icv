import PositionId from './PositionId';
import PositionName from './PositionName';
import PositionColor from './PositionColor';
import PositionSpecialization from './PositionSpecialization';

type Position = {
  id: PositionId;
  name: PositionName;
  color: PositionColor;
  specialization: PositionSpecialization;
};

export default Position;
