import SkillId from '../skill/SkillId';
import SkillName from '../skill/SkillName';
import SkillAvatar from '../skill/SkillAvatar';
import SkillColor from '../skill/SkillColor';
import SkillTag from '../skill/SkillTag';
import SkillExperience from '../skill/SkillExperience';

type Experience = {
  id: SkillId;
  name: SkillName;
  avatar?: SkillAvatar;
  color?: SkillColor;
  tags?: SkillTag[];
  experience: SkillExperience;
};

export default Experience;
