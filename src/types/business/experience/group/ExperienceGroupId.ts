import NumberCommon from '../../../common/NumberCommon';

export const DEFAULT_EXPERIENCE_GROUP_ID: ExperienceGroupId = -1;

type ExperienceGroupId = NumberCommon;

export default ExperienceGroupId;
