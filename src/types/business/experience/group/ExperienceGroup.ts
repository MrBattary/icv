import ExperienceGroupId from './ExperienceGroupId';
import TagGroup from '../../tag/group/TagGroup';
import Experience from '../Experience';

type ExperienceGroup = {
  id: ExperienceGroupId;
  tagGroup: TagGroup;
  experiences: Experience[];
};

export default ExperienceGroup;
