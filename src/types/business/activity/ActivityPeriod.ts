import SinglePeriod from '../../../tools/timedate/period/SinglePeriod';

type ActivityPeriod = SinglePeriod;

export default ActivityPeriod;
