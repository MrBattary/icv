import GeolocationPlace from '../../../tools/geolocation/GeolocationPlace';

type UserLocation = GeolocationPlace;

export default UserLocation;
