import NumberDate from '../../../tools/timedate/date/NumberDate';

type UserBirthDate = Pick<NumberDate, 'year'> &
  Partial<Pick<NumberDate, 'month' | 'day'>>;

export default UserBirthDate;
