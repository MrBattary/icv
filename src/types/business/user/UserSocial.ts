import UserContact from './UserContact';
import UserLink from './UserLink';

type UserSocial = {
  contacts: UserContact[];
  links: UserLink[];
};

export default UserSocial;
