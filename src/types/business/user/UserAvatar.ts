import ImageCommon from '../../common/media/image/ImageCommon';

type UserAvatar = ImageCommon;

export default UserAvatar;
