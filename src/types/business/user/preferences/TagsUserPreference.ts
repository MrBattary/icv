import TagGroup from '../../tag/group/TagGroup';

type TagsUserPreference = TagGroup[];

export default TagsUserPreference;
