import ContactsUserPreference from './ContactsUserPreference';
import TagsUserPreference from './TagsUserPreference';

type UserPreferences = {
  contacts?: ContactsUserPreference;
  tagGroups?: TagsUserPreference;
};

export default UserPreferences;
