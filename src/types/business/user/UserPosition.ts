import Position from '../position/Position';

type UserPosition = Position;

export default UserPosition;
