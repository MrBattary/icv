import SocialLink from '../social/link/SocialLink';

type UserLink = SocialLink;

export default UserLink;
