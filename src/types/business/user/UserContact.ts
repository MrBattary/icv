import SocialContact from '../social/contact/SocialContact';

type UserContact = SocialContact;

export default UserContact;
