import CommonSocialLink from './variants/CommonSocialLink';
import GitLabSocialLink from './variants/GitLabSocialLink';
import GitHubSocialLink from './variants/GitHubSocialLink';
import LinkedInSocialLink from './variants/LinkedInSocialLink';

type SocialLink =
  | CommonSocialLink
  | GitLabSocialLink
  | GitHubSocialLink
  | LinkedInSocialLink;

export default SocialLink;
