import UrlPath from '../../../../../tools/web/url/paths/UrlPath';
import SocialLinkType, { COMMON_SOCIAL_LINK_TYPE } from '../SocialLinkType';

type SocialLinkPrototype<
  T extends Exclude<SocialLinkType, typeof COMMON_SOCIAL_LINK_TYPE>,
> = {
  type: T;
  path: UrlPath;
};

export default SocialLinkPrototype;
