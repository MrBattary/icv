import SocialLinkPrototype from '../prototype/SocialLinkPrototype';
import { LINKEDIN_SOCIAL_LINK_TYPE } from '../SocialLinkType';

type LinkedInSocialLink = SocialLinkPrototype<typeof LINKEDIN_SOCIAL_LINK_TYPE>;

export default LinkedInSocialLink;
