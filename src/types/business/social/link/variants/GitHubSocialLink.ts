import SocialLinkPrototype from '../prototype/SocialLinkPrototype';
import { GITHUB_SOCIAL_LINK_TYPE } from '../SocialLinkType';

type GitHubSocialLink = SocialLinkPrototype<typeof GITHUB_SOCIAL_LINK_TYPE>;

export default GitHubSocialLink;
