import Url from '../../../../../tools/web/url/Url';
import { COMMON_SOCIAL_LINK_TYPE } from '../SocialLinkType';

type CommonSocialLink = {
  type: typeof COMMON_SOCIAL_LINK_TYPE;
  url: Url;
};

export default CommonSocialLink;
