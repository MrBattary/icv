import SocialLinkPrototype from '../prototype/SocialLinkPrototype';
import { GITLAB_SOCIAL_LINK_TYPE } from '../SocialLinkType';

type GitLabSocialLink = SocialLinkPrototype<typeof GITLAB_SOCIAL_LINK_TYPE>;

export default GitLabSocialLink;
