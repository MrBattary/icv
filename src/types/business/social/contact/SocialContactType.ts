export const EMAIL_SOCIAL_CONTACT_TYPE = 'email';
export const TELEGRAM_SOCIAL_CONTACT_TYPE = 'telegram';

type SocialContactType =
  | typeof EMAIL_SOCIAL_CONTACT_TYPE
  | typeof TELEGRAM_SOCIAL_CONTACT_TYPE;

export default SocialContactType;
