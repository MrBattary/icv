import EmailSocialContact from './variants/EmailSocialContact';
import TelegramSocialContact from './variants/TelegramSocialContact';

type SocialContact = EmailSocialContact | TelegramSocialContact;

export default SocialContact;
