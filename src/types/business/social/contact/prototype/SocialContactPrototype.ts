import SocialContactType from '../SocialContactType';
import SocialContactValue from '../SocialContactValue';

type SocialContactPrototype<T extends SocialContactType> = {
  type: T;
  value: SocialContactValue;
};

export default SocialContactPrototype;
