import SocialContactPrototype from '../prototype/SocialContactPrototype';
import { TELEGRAM_SOCIAL_CONTACT_TYPE } from '../SocialContactType';

type TelegramSocialContact = SocialContactPrototype<
  typeof TELEGRAM_SOCIAL_CONTACT_TYPE
>;

export default TelegramSocialContact;
