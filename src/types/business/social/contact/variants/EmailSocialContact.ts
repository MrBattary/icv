import SocialContactPrototype from '../prototype/SocialContactPrototype';
import { EMAIL_SOCIAL_CONTACT_TYPE } from '../SocialContactType';

type EmailSocialContact = SocialContactPrototype<
  typeof EMAIL_SOCIAL_CONTACT_TYPE
>;

export default EmailSocialContact;
