import SocialLink from '../social/link/SocialLink';

type ProjectLink = SocialLink;

export default ProjectLink;
