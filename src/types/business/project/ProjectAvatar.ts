import ImageCommon from '../../common/media/image/ImageCommon';

type ProjectAvatar = ImageCommon;

export default ProjectAvatar;
