import ImageCommon from '../../common/media/image/ImageCommon';

type ProviderAvatar = ImageCommon;

export default ProviderAvatar;
