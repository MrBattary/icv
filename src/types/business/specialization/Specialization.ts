import SpecializationId from './SpecializationId';
import SpecializationName from './SpecializationName';
import SpecializationColor from './SpecializationColor';

type Specialization = {
  id: SpecializationId;
  name: SpecializationName;
  color: SpecializationColor;
};

export default Specialization;
