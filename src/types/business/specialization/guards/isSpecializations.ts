import Specialization from '../Specialization';
import isSpecialization from './isSpecialization';
import isArray from '../../../common/guards/isArray';

const isSpecializations = (
  specializations: unknown,
): specializations is Specialization[] =>
  isArray(specializations) &&
  specializations.length > 0 &&
  specializations.every((specialization) => isSpecialization(specialization));

export default isSpecializations;
