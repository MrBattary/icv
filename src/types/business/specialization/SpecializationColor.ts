import HexColorCommon from '../../common/media/color/hex/HexColorCommon';

type SpecializationColor = HexColorCommon;

export default SpecializationColor;
