import HexColorCommon from '../../common/media/color/hex/HexColorCommon';

type SkillColor = HexColorCommon;

export default SkillColor;
