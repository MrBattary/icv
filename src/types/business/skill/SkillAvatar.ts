import ImageCommon from '../../common/media/image/ImageCommon';

type SkillAvatar = ImageCommon;

export default SkillAvatar;
