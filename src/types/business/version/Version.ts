import TextCommon from '../../common/TextCommon';
import VersionResource from '../../../tools/resources/types/business/version/VersionResource';

type Version = TextCommon;

export const createVersionFromResource = (
  versionResource: VersionResource,
): Version => versionResource;

export default Version;
