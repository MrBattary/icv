import FilterDataPrototype from '../../prototype/FilterDataPrototype';
import Position from '../../../../position/Position';
import { ACTIVITY_POSITION_FILTER } from '../../prototype/fields/FilterType';

type ActivityPositionFilterData = FilterDataPrototype<
  typeof ACTIVITY_POSITION_FILTER,
  Position
>;

export default ActivityPositionFilterData;
