import FilterDataPrototype from '../../prototype/FilterDataPrototype';
import Specialization from '../../../../specialization/Specialization';
import { POSITION_SPECIALIZATION_FILTER } from '../../prototype/fields/FilterType';

type PositionSpecializationFilterData = FilterDataPrototype<
  typeof POSITION_SPECIALIZATION_FILTER,
  Specialization
>;

export default PositionSpecializationFilterData;
