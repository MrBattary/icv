import FilterDataPrototype from '../../prototype/FilterDataPrototype';
import ActivityType from '../../../../activity/ActivityType';
import { ACTIVITY_TYPE_FILTER } from '../../prototype/fields/FilterType';

type ActivityTypeFilterData = FilterDataPrototype<
  typeof ACTIVITY_TYPE_FILTER,
  ActivityType
>;

export default ActivityTypeFilterData;
