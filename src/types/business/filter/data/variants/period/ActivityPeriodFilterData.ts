import FilterDataPrototype from '../../prototype/FilterDataPrototype';
import ActivityPeriodFilterValue from './value/ActivityPeriodFilterValue';
import { ACTIVITY_PERIOD_FILTER } from '../../prototype/fields/FilterType';

type ActivityPeriodFilterData = FilterDataPrototype<
  typeof ACTIVITY_PERIOD_FILTER,
  ActivityPeriodFilterValue
>;

export default ActivityPeriodFilterData;
