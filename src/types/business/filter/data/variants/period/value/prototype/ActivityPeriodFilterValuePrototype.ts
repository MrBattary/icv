import ActivityPeriodFilterValueKey from '../ActivityPeriodFilterValueKey';
import ActivityPeriodFilterValueParameter from '../ActivityPeriodFilterValueParameter';

type ActivityPeriodFilterValuePrototype<
  K extends ActivityPeriodFilterValueKey,
  V extends ActivityPeriodFilterValueParameter,
> = {
  key: K;
  value: V;
};

export default ActivityPeriodFilterValuePrototype;
