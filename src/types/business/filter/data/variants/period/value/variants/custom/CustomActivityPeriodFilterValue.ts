import CustomActivityPeriodFilterValueParameter from './CustomActivityPeriodFilterValueParameter';
import ActivityPeriodFilterValuePrototype from '../../prototype/ActivityPeriodFilterValuePrototype';
import { ACTIVITY_PERIOD_FILTER_VALUE_CUSTOM_KEY } from '../../ActivityPeriodFilterValueKey';

type CustomActivityPeriodFilterValue = ActivityPeriodFilterValuePrototype<
  typeof ACTIVITY_PERIOD_FILTER_VALUE_CUSTOM_KEY,
  CustomActivityPeriodFilterValueParameter
>;

export default CustomActivityPeriodFilterValue;
