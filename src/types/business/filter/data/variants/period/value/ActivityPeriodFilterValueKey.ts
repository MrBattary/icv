import PredefinedActivityPeriodFilterValueKey from './variants/predefined/PredefinedActivityPeriodFilterValueKey';

export const ACTIVITY_PERIOD_FILTER_VALUE_CUSTOM_KEY = 'CUSTOM';

type ActivityPeriodFilterValueKey =
  | PredefinedActivityPeriodFilterValueKey
  | typeof ACTIVITY_PERIOD_FILTER_VALUE_CUSTOM_KEY;

export default ActivityPeriodFilterValueKey;
