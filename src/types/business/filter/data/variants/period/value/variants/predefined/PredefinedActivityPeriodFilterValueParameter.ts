import Duration from '../../../../../../../../../tools/timedate/duration/Duration';

type PredefinedActivityPeriodFilterValueParameter = Duration;

export default PredefinedActivityPeriodFilterValueParameter;
