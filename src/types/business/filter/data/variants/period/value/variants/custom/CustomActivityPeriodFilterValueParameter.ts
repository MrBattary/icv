import SinglePeriod from '../../../../../../../../../tools/timedate/period/SinglePeriod';

type CustomActivityPeriodFilterValueParameter = Partial<SinglePeriod>;

export default CustomActivityPeriodFilterValueParameter;
