import CustomActivityPeriodFilterValue from './variants/custom/CustomActivityPeriodFilterValue';
import PredefinedActivityPeriodFilterValue from './variants/predefined/PredefinedActivityPeriodFilterValue';

type ActivityPeriodFilterValue =
  | PredefinedActivityPeriodFilterValue
  | CustomActivityPeriodFilterValue;

export default ActivityPeriodFilterValue;
