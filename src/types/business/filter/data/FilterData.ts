import ActivityTypeFilterData from './variants/type/ActivityTypeFilterData';
import ActivityPeriodFilterData from './variants/period/ActivityPeriodFilterData';
import ActivityPositionFilterData from './variants/position/ActivityPositionFilterData';
import PositionSpecializationFilterData from './variants/specialization/PositionSpecializationFilterData';

type FilterData =
  | ActivityTypeFilterData
  | ActivityPositionFilterData
  | PositionSpecializationFilterData
  | ActivityPeriodFilterData;

export default FilterData;
