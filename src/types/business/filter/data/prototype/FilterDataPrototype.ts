import FilterType from './fields/FilterType';

type FilterDataPrototype<T extends FilterType, V> = {
  type: T;
  data: V;
};

export default FilterDataPrototype;
