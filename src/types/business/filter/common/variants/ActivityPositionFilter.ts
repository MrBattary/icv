import FilterPrototype from '../prototype/FilterPrototype';
import ActivityPositionFilterData from '../../data/variants/position/ActivityPositionFilterData';

type ActivityPositionFilter = FilterPrototype<ActivityPositionFilterData>;

export default ActivityPositionFilter;
