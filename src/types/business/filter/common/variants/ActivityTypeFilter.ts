import FilterPrototype from '../prototype/FilterPrototype';
import ActivityTypeFilterData from '../../data/variants/type/ActivityTypeFilterData';

type ActivityTypeFilter = FilterPrototype<ActivityTypeFilterData>;

export default ActivityTypeFilter;
