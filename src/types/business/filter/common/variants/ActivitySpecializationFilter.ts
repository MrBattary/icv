import FilterPrototype from '../prototype/FilterPrototype';
import PositionSpecializationFilterData from '../../data/variants/specialization/PositionSpecializationFilterData';

type ActivitySpecializationFilter =
  FilterPrototype<PositionSpecializationFilterData>;

export default ActivitySpecializationFilter;
