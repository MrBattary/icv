import FilterPrototype from '../prototype/FilterPrototype';
import ActivityPeriodFilterData from '../../data/variants/period/ActivityPeriodFilterData';

type ActivityPeriodFilter = FilterPrototype<ActivityPeriodFilterData>;

export default ActivityPeriodFilter;
