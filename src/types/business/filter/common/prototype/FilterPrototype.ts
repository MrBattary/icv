import FilterId from './fields/FilterId';
import FilterDataPrototype from '../../data/prototype/FilterDataPrototype';
import FilterType from '../../data/prototype/fields/FilterType';

type FilterPrototype<T extends FilterDataPrototype<FilterType, unknown>> = T & {
  id: FilterId;
};

export default FilterPrototype;
