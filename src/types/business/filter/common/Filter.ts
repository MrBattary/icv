import ActivityPeriodFilter from './variants/ActivityPeriodFilter';
import ActivityTypeFilter from './variants/ActivityTypeFilter';
import ActivityPositionFilter from './variants/ActivityPositionFilter';
import ActivitySpecializationFilter from './variants/ActivitySpecializationFilter';

type Filter =
  | ActivityPeriodFilter
  | ActivityPositionFilter
  | ActivitySpecializationFilter
  | ActivityTypeFilter;

export default Filter;
