import CvUser from './fields/user/CvUser';
import CvActivity from './fields/activity/CvActivity';

type Cv = {
  user?: CvUser;
  activities?: CvActivity[];
};

export default Cv;
