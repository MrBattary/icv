import SpecializationId from '../../../../../../../specialization/SpecializationId';

type CvUserPositionSpecializationId = SpecializationId;

export default CvUserPositionSpecializationId;
