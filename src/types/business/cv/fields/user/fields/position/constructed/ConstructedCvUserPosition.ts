import Position from '../../../../../../position/Position';
import CvUserPositionSpecializationId from './fields/CvUserPositionSpecializationId';
import CvUserPositionSpecialization from '../fields/specialization/CvUserPositionSpecialization';

type ConstructedCvUserPosition = Omit<Position, 'specialization'> & {
  specialization: CvUserPositionSpecializationId | CvUserPositionSpecialization;
};

export default ConstructedCvUserPosition;
