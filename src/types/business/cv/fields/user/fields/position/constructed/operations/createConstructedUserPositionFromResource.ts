import ProcessedPositionResource from '../../../../../../../../../tools/resources/types/business/positions/processed/ProcessedPositionResource';
import ConstructedCvUserPosition from '../ConstructedCvUserPosition';

const createConstructedUserPositionFromResource = (
  positionResource?: ProcessedPositionResource,
): ConstructedCvUserPosition | undefined =>
  positionResource && {
    id: positionResource.id,
    name: positionResource.name,
    color: positionResource.color,
    specialization: positionResource.specializationId,
  };

export default createConstructedUserPositionFromResource;
