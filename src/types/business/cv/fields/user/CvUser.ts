import UserId from '../../../user/UserId';
import UserLocation from '../../../user/UserLocation';
import UserAvatar from '../../../user/UserAvatar';
import UserFirstName from '../../../user/UserFirstName';
import UserMiddleName from '../../../user/UserMiddleName';
import UserLastName from '../../../user/UserLastName';
import UserAge from '../../../user/UserAge';
import UserBirthDate from '../../../user/UserBirthDate';
import CvUserPosition from './fields/position/CvUserPosition';
import UserSocial from '../../../user/UserSocial';
import UserPreferences from '../../../user/preferences/UserPreferences';

type CvUser = {
  id: UserId;
  avatar?: UserAvatar;
  firstName: UserFirstName;
  middleName?: UserMiddleName;
  lastName?: UserLastName;
  age?: UserAge;
  birthdate?: UserBirthDate;
  position?: CvUserPosition;
  location?: UserLocation;
  social?: UserSocial;
  preferences?: UserPreferences;
};

export default CvUser;
