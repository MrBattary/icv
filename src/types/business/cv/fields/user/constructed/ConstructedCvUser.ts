import CvUser from '../CvUser';
import CvUserPositionId from './fields/CvUserPositionId';
import ConstructedCvUserPosition from '../fields/position/constructed/ConstructedCvUserPosition';
import ContactsUserPreference from '../../../../user/preferences/ContactsUserPreference';
import CvUserPreferencesTagGroup from './fields/CvUserPreferencesTagGroup';
import TagsUserPreference from '../../../../user/preferences/TagsUserPreference';

type ConstructedCvUser = Omit<CvUser, 'position' | 'preferences'> & {
  position?: CvUserPositionId | ConstructedCvUserPosition;
  preferences?: {
    contacts?: ContactsUserPreference;
    tagGroups?: CvUserPreferencesTagGroup[] | TagsUserPreference;
  };
};

export default ConstructedCvUser;
