import PositionId from '../../../../../position/PositionId';

type CvUserPositionId = PositionId;

export default CvUserPositionId;
