import TagGroupName from '../../../../../tag/group/TagGroupName';
import TagId from '../../../../../tag/TagId';

type CvUserPreferencesTagGroup = {
  name?: TagGroupName;
  tags: TagId[];
};

export default CvUserPreferencesTagGroup;
