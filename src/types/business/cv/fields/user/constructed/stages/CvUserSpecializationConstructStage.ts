import DemandingConstructStage from '../../../../../../../tools/constructed/stages/DemandingConstructStage';
import {
  CV_USER_POSITIONS_BUILD_STAGE_NAME,
  CV_USER_SPECIALIZATIONS_BUILD_STAGE_NAME,
} from '../CvUserConstructStageName';

type CvUserSpecializationConstructStage = DemandingConstructStage<
  typeof CV_USER_SPECIALIZATIONS_BUILD_STAGE_NAME,
  typeof CV_USER_POSITIONS_BUILD_STAGE_NAME
>;

export const CV_USER_SPECIALIZATION_CONSTRUCT_STAGE: CvUserSpecializationConstructStage =
  {
    name: CV_USER_SPECIALIZATIONS_BUILD_STAGE_NAME,
    require: [CV_USER_POSITIONS_BUILD_STAGE_NAME],
  };
