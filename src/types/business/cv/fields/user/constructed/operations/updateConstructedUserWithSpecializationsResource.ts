import ConstructedCvUser from '../ConstructedCvUser';
import ProcessedSpecializationsResource from '../../../../../../../tools/resources/types/business/specializations/processed/ProcessedSpecializationsResource';
import ConstructedCvUserPosition from '../../fields/position/constructed/ConstructedCvUserPosition';

import updateConstructedUserPositionWithSpecializationsResource from '../../fields/position/constructed/operations/updateConstructedUserPositionWithSpecializationsResource';

const updateConstructedUserWithSpecializationsResource = (
  constructedUser: ConstructedCvUser,
  specializationsResource: ProcessedSpecializationsResource,
): ConstructedCvUser => ({
  ...constructedUser,
  position: updateConstructedUserPositionWithSpecializationsResource(
    constructedUser.position as ConstructedCvUserPosition,
    specializationsResource,
  ),
});

export default updateConstructedUserWithSpecializationsResource;
