import ConstructedCvUser from '../ConstructedCvUser';
import CvUserPositionId from '../fields/CvUserPositionId';
import ProcessedPositionsResource from '../../../../../../../tools/resources/types/business/positions/processed/ProcessedPositionsResource';

import createConstructedUserPositionFromResource from '../../fields/position/constructed/operations/createConstructedUserPositionFromResource';

const updateConstructedUserWithPositionsResource = (
  constructedUser: ConstructedCvUser,
  positionsResource: ProcessedPositionsResource,
): ConstructedCvUser => ({
  ...constructedUser,
  position: createConstructedUserPositionFromResource(
    positionsResource.find(
      (position) =>
        position.id === (constructedUser?.position as CvUserPositionId),
    ),
  ),
});

export default updateConstructedUserWithPositionsResource;
