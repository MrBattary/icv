import ConstructedCvActivity from '../ConstructedCvActivity';
import ProcessedPositionsResource from '../../../../../../../tools/resources/types/business/positions/processed/ProcessedPositionsResource';
import CvActivityPositionId from '../fields/CvActivityPositionId';

import createConstructedActivityPositionFromResource from '../../fields/position/constructed/operations/createConstructedActivityPositionFromResource';

const updateConstructedActivityWithPositionResource = (
  constructedActivity: ConstructedCvActivity,
  positionsResource: ProcessedPositionsResource,
): ConstructedCvActivity => ({
  ...constructedActivity,
  position: createConstructedActivityPositionFromResource(
    positionsResource.find(
      (position) =>
        position.id === (constructedActivity.position as CvActivityPositionId),
    ),
  ),
});

export default updateConstructedActivityWithPositionResource;
