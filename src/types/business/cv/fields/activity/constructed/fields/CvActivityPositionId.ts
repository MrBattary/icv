import PositionId from '../../../../../position/PositionId';

type CvActivityPositionId = PositionId;

export default CvActivityPositionId;
