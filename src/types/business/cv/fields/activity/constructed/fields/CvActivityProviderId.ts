import ProviderId from '../../../../../provider/ProviderId';

type CvActivityProviderId = ProviderId;

export default CvActivityProviderId;
