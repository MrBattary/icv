import SubactivityId from '../../../../../subactivity/SubactivityId';

type CvActivitySubactivityId = SubactivityId;

export default CvActivitySubactivityId;
