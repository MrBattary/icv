import CvActivity from '../CvActivity';
import CvActivityPositionId from './fields/CvActivityPositionId';
import CvActivityProjectId from './fields/CvActivityProjectId';
import CvActivityProviderId from './fields/CvActivityProviderId';
import CvActivitySubactivityId from './fields/CvActivitySubactivityId';
import CvActivityProject from '../fields/project/CvActivityProject';
import CvActivityProvider from '../fields/provider/CvActivityProvider';
import ConstructedCvActivityPosition from '../fields/position/constructed/ConstructedCvActivityPosition';
import ConstructedCvActivitySubactivity from '../fields/subactivities/constructed/ConstructedCvActivitySubactivity';

type ConstructedCvActivity = Pick<
  CvActivity,
  'id' | 'heirId' | 'type' | 'period'
> & {
  position?: CvActivityPositionId | ConstructedCvActivityPosition;
  project?: CvActivityProjectId | CvActivityProject;
  provider?: CvActivityProviderId | CvActivityProvider;
  subactivities?: (
    | CvActivitySubactivityId
    | ConstructedCvActivitySubactivity
  )[];
};

export default ConstructedCvActivity;
