import DemandingConstructStage from '../../../../../../../tools/constructed/stages/DemandingConstructStage';
import {
  CV_ACTIVITY_BASE_CONSTRUCT_STAGE_NAME,
  CV_ACTIVITY_PROJECT_CONSTRUCT_STAGE_NAME,
} from '../CvActivitiesConstructStageName';

type CvActivityProjectConstructStage = DemandingConstructStage<
  typeof CV_ACTIVITY_PROJECT_CONSTRUCT_STAGE_NAME,
  typeof CV_ACTIVITY_BASE_CONSTRUCT_STAGE_NAME
>;

export const CV_ACTIVITY_PROJECT_CONSTRUCT_STAGE: CvActivityProjectConstructStage =
  {
    name: CV_ACTIVITY_PROJECT_CONSTRUCT_STAGE_NAME,
    require: [CV_ACTIVITY_BASE_CONSTRUCT_STAGE_NAME],
  };
