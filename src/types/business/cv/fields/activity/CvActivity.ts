import ActivityId from '../../../activity/ActivityId';
import ActivityType from '../../../activity/ActivityType';
import ActivityPeriod from '../../../activity/ActivityPeriod';
import CvActivityProject from './fields/project/CvActivityProject';
import CvActivityProvider from './fields/provider/CvActivityProvider';
import CvActivityPosition from './fields/position/CvActivityPosition';
import CvActivitySubactivity from './fields/subactivities/CvActivitySubactivity';

type CvActivity = {
  id: ActivityId;
  heirId?: ActivityId;
  type: ActivityType;
  period: ActivityPeriod;
  position?: CvActivityPosition;
  project?: CvActivityProject;
  provider?: CvActivityProvider;
  subactivities?: CvActivitySubactivity[];
};

export default CvActivity;
