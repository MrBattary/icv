import SpecializationId from '../../../../../../../specialization/SpecializationId';

type CvActivityPositionSpecializationId = SpecializationId;

export default CvActivityPositionSpecializationId;
