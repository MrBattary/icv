import Position from '../../../../../../position/Position';
import CvActivityPositionSpecializationId from './fields/CvActivityPositionSpecializationId';
import CvActivityPositionSpecialization from '../fields/specialization/CvActivityPositionSpecialization';

type ConstructedCvActivityPosition = Omit<Position, 'specialization'> & {
  specialization:
    | CvActivityPositionSpecializationId
    | CvActivityPositionSpecialization;
};

export default ConstructedCvActivityPosition;
