import CvActivitySubactivity from '../CvActivitySubactivity';
import CvActivitySubactivitySkillId from './fields/CvActivitySubactivitySkillId';
import ConstructedCvSubactivitySkill from '../fields/skills/constructed/ConstructedCvSubactivitySkill';

type ConstructedCvActivitySubactivity = Omit<
  CvActivitySubactivity,
  'skills'
> & {
  skills?: (CvActivitySubactivitySkillId | ConstructedCvSubactivitySkill)[];
};

export default ConstructedCvActivitySubactivity;
