import CvSkillTag from '../CvSkillTag';
import isTag from '../../../../../../../../../../tag/guards/isTag';

const isCvSkillTag = (cvSkillTag: unknown): cvSkillTag is CvSkillTag =>
  isTag(cvSkillTag);

export default isCvSkillTag;
