import SkillId from '../../../../../../../skill/SkillId';

type CvActivitySubactivitySkillId = SkillId;

export default CvActivitySubactivitySkillId;
