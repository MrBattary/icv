import SkillTag from '../../../../../../../../../skill/SkillTag';
import TagResource from '../../../../../../../../../../../tools/resources/types/business/tags/TagResource';

type CvSkillTag = SkillTag;

export const createTagFromResource = (resource: TagResource): CvSkillTag => ({
  id: resource.id,
  name: resource.name,
});

export default CvSkillTag;
