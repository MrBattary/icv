import CvSubactivitySkill from '../CvSubactivitySkill';
import CvSkillTagId from './fields/CvSkillTagId';
import CvSkillTag from '../fields/tags/CvSkillTag';

type ConstructedCvSubactivitySkill = Omit<CvSubactivitySkill, 'tags'> & {
  tags?: (CvSkillTagId | CvSkillTag)[];
};

export default ConstructedCvSubactivitySkill;
