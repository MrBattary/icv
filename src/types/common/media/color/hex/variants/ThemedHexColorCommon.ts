import Themed from '../../../Themed';
import NonThemedHexColorCommon from './NonThemedHexColorCommon';

type ThemedHexColorCommon = Themed<NonThemedHexColorCommon>;

export default ThemedHexColorCommon;
