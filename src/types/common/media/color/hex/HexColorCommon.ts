import ThemedHexColorCommon from './variants/ThemedHexColorCommon';
import NonThemedHexColorCommon from './variants/NonThemedHexColorCommon';

type HexColorCommon = ThemedHexColorCommon | NonThemedHexColorCommon;

export default HexColorCommon;
