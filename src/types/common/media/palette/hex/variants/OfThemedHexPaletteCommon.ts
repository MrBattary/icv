import ThemedHexColorCommon from '../../../color/hex/variants/ThemedHexColorCommon';

type OfThemedHexPaletteCommon = ThemedHexColorCommon[];

export default OfThemedHexPaletteCommon;
