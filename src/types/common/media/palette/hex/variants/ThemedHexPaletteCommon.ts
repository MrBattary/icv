import Themed from '../../../Themed';
import NonThemedHexColorCommon from '../../../color/hex/variants/NonThemedHexColorCommon';

type ThemedHexPaletteCommon = Themed<NonThemedHexColorCommon[]>;

export default ThemedHexPaletteCommon;
