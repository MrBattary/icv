import NonThemedHexColorCommon from '../../../color/hex/variants/NonThemedHexColorCommon';

type NonThemedHexPaletteCommon = NonThemedHexColorCommon[];

export default NonThemedHexPaletteCommon;
