type Themed<T> = {
  light: T;
  dark: T;
};

export default Themed;
