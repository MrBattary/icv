import ImageCommonSource from '../fields/source/ImageCommonSource';
import ImageCommonShape from '../fields/shape/ImageCommonShape';

type ImageCommonPrototype<T extends ImageCommonSource> = {
  src: T;
  alt?: string;
  shape?: ImageCommonShape;
};

export default ImageCommonPrototype;
