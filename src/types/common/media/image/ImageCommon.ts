import NonThemedImageCommon from './variants/NonThemedImageCommon';
import ThemedImageCommon from './variants/ThemedImageCommon';

type ImageCommon = NonThemedImageCommon | ThemedImageCommon;

export default ImageCommon;
