import ImageCommonPrototype from '../prototype/ImageCommonPrototype';
import ImageCommonThemedSource from '../fields/source/variants/ImageCommonThemedSource';

type ThemedImageCommon = ImageCommonPrototype<ImageCommonThemedSource>;

export default ThemedImageCommon;
