import ImageCommonPrototype from '../prototype/ImageCommonPrototype';
import ImageCommonNonThemedSource from '../fields/source/variants/ImageCommonNonThemedSource';

type NonThemedImageCommon = ImageCommonPrototype<ImageCommonNonThemedSource>;

export default NonThemedImageCommon;
