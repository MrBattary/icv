import ImageCommon from '../ImageCommon';
import ThemedImageCommon from '../variants/ThemedImageCommon';
import isImageSourceThemed from '../fields/source/guards/isImageSourceThemed';

const isImageCommonThemed = (image: ImageCommon): image is ThemedImageCommon =>
  isImageSourceThemed(image.src);

export default isImageCommonThemed;
