import ImageCommonNonThemedSource from './variants/ImageCommonNonThemedSource';
import ImageCommonThemedSource from './variants/ImageCommonThemedSource';

type ImageCommonSource = ImageCommonNonThemedSource | ImageCommonThemedSource;

export default ImageCommonSource;
