import Themed from '../../../../Themed';
import ImageCommonNonThemedSource from './ImageCommonNonThemedSource';

type ImageCommonThemedSource = Themed<ImageCommonNonThemedSource>;

export default ImageCommonThemedSource;
