import Themed from '../Themed';

const isThemed = <T>(media: T | Themed<T>): media is Themed<T> =>
  (media as Themed<T>).light !== undefined &&
  (media as Themed<T>).dark !== undefined;

export default isThemed;
