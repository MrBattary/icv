import Url from '../../tools/web/url/Url';

type LinkCommon = {
  url: Url;
};

export default LinkCommon;
