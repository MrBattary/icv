import { useDispatch } from 'react-redux';

import { STORE } from '../store/store';

export type RootDispatch = typeof STORE.dispatch;

const useRootDispatch: () => RootDispatch = useDispatch;

export default useRootDispatch;
