import { configureStore } from '@reduxjs/toolkit';

import { rootReducer } from '../reducers/rootReducer';

export const STORE = configureStore({
  reducer: rootReducer,
});
