import Loadable from './Loadable';

type LoadableValue<V> = Loadable & { value: V };

export default LoadableValue;
