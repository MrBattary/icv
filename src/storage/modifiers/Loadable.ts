type Loadable = {
  loading: boolean;
};

export default Loadable;
