import requireResource, { RequireResource } from '../requireResource';
import { LOAD_SUBACTIVITIES_RESOURCE_REQUIREMENT } from '../resourceActionTypes';

export type RequireSubactivitiesResource = RequireResource<
  typeof LOAD_SUBACTIVITIES_RESOURCE_REQUIREMENT
>;

const requireSubactivitiesResource = () =>
  requireResource(LOAD_SUBACTIVITIES_RESOURCE_REQUIREMENT);

export default requireSubactivitiesResource;
