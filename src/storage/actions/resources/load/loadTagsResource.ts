import loadResource, { LoadResource } from '../loadResource';

import I18nLanguage from '../../../../tools/internationalization/i18n/types/I18nLanguage';
import TagsResource from '../../../../tools/resources/types/business/tags/TagsResource';

import {
  LOAD_TAGS_RESOURCE_ERROR,
  LOAD_TAGS_RESOURCE_REQUEST,
  LOAD_TAGS_RESOURCE_RESPONSE,
} from '../resourceActionTypes';
import RESOURCES_API from '../../../../tools/api/resources';

export type LoadTagsResource = LoadResource<
  typeof LOAD_TAGS_RESOURCE_REQUEST,
  typeof LOAD_TAGS_RESOURCE_RESPONSE,
  typeof LOAD_TAGS_RESOURCE_ERROR,
  TagsResource
>;

const loadTagsResource = (language: I18nLanguage) =>
  loadResource(
    LOAD_TAGS_RESOURCE_REQUEST,
    LOAD_TAGS_RESOURCE_RESPONSE,
    LOAD_TAGS_RESOURCE_ERROR,
    () => RESOURCES_API.getTags(language),
  );

export default loadTagsResource;
