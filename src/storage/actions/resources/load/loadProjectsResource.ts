import loadResource, { LoadResource } from '../loadResource';

import I18nLanguage from '../../../../tools/internationalization/i18n/types/I18nLanguage';
import ProjectsResource from '../../../../tools/resources/types/business/projects/ProjectsResource';

import {
  LOAD_PROJECTS_RESOURCE_ERROR,
  LOAD_PROJECTS_RESOURCE_REQUEST,
  LOAD_PROJECTS_RESOURCE_RESPONSE,
} from '../resourceActionTypes';
import RESOURCES_API from '../../../../tools/api/resources';

export type LoadProjectsResource = LoadResource<
  typeof LOAD_PROJECTS_RESOURCE_REQUEST,
  typeof LOAD_PROJECTS_RESOURCE_RESPONSE,
  typeof LOAD_PROJECTS_RESOURCE_ERROR,
  ProjectsResource
>;

const loadProjectsResource = (language: I18nLanguage) =>
  loadResource(
    LOAD_PROJECTS_RESOURCE_REQUEST,
    LOAD_PROJECTS_RESOURCE_RESPONSE,
    LOAD_PROJECTS_RESOURCE_ERROR,
    () => RESOURCES_API.getProjects(language),
  );

export default loadProjectsResource;
