import { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../../storage/reducers/rootReducer';

import useStaticResourcesLoader from '../../resources/useStaticResourcesLoader';
import useStaticResources from '../../resources/useStaticResources';

import CvActivity from '../../../types/business/cv/fields/activity/CvActivity';

import isConstructionFinished from '../../../tools/constructed/utils/isConstructionFinished';
import toCvActivities from '../../../types/business/cv/fields/activity/constructed/converters/toCvActivities';

interface UseCvLoader {
  loadCv: () => void;
  isCvUserLoaded: boolean;
  isCvActivitiesLoaded: boolean;
  initialCvActivities: CvActivity[];
}

/**
 * @see useCvLoaderEffects - Receiver of the useCvLoader events
 */
const useCvLoader = (): UseCvLoader => {
  const { loadCvResource } = useStaticResourcesLoader();
  const { cv: cvResource } = useStaticResources();
  const { cvUser, cvActivities } = useSelector(
    (state: RootState) => state.cvReducer,
  );

  // Initial point of loading
  const loadCv = () => {
    if (!cvResource) {
      loadCvResource();
    }
  };

  // Other
  const isCvUserLoaded = useMemo(
    () => isConstructionFinished(cvUser.initial),
    [cvUser.initial],
  );

  const isCvActivitiesLoaded = useMemo(
    () => isConstructionFinished(cvActivities.initial),
    [cvActivities.initial],
  );

  const initialCvActivitiesValue = useMemo(
    () => toCvActivities(cvActivities.initial.value) ?? [],
    [cvActivities.initial.value],
  );

  return {
    loadCv,
    isCvUserLoaded,
    isCvActivitiesLoaded,
    initialCvActivities: initialCvActivitiesValue,
  };
};

export default useCvLoader;
