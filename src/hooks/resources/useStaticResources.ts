import { useSelector } from 'react-redux';

import { RootState } from '../../storage/reducers/rootReducer';

import LoadableResource from '../../tools/resources/types/LoadableResource';
import VersionResource from '../../tools/resources/types/business/version/VersionResource';
import LanguageBundles from '../../types/business/languageBundles/LanguageBundles';
import CvResource from '../../tools/resources/types/business/cv/CvResource';
import UserResource from '../../tools/resources/types/business/users/UserResource';
import ActivitiesResource from '../../tools/resources/types/business/activities/ActivitiesResource';
import SubactivitiesResource from '../../tools/resources/types/business/subactivities/SubactivitiesResource';
import ProjectsResource from '../../tools/resources/types/business/projects/ProjectsResource';
import ProvidersResource from '../../tools/resources/types/business/providers/ProvidersResource';
import PositionsResource from '../../tools/resources/types/business/positions/PositionsResource';
import ProcessedPositionsResource from '../../tools/resources/types/business/positions/processed/ProcessedPositionsResource';
import SkillsResource from '../../tools/resources/types/business/skills/SkillsResource';
import ProcessedSkillsResource from '../../tools/resources/types/business/skills/processed/ProcessedSkillsResource';
import TagsResource from '../../tools/resources/types/business/tags/TagsResource';
import SpecializationsResource from '../../tools/resources/types/business/specializations/SpecializationsResource';
import ProcessedSpecializationsResource from '../../tools/resources/types/business/specializations/processed/ProcessedSpecializationsResource';

interface UseStaticResources {
  version: LoadableResource<VersionResource>;
  versionLoading: boolean;
  languageBundles: LoadableResource<LanguageBundles>;
  languageBundlesLoading: boolean;
  cv: LoadableResource<CvResource>;
  cvBase: LoadableResource<CvResource>;
  cvLoading: boolean;
  user: LoadableResource<UserResource>;
  userBase: LoadableResource<UserResource>;
  userLoading: boolean;
  activities: LoadableResource<ActivitiesResource>;
  activitiesBase: LoadableResource<ActivitiesResource>;
  activitiesLoading: boolean;
  subactivities: LoadableResource<SubactivitiesResource>;
  subactivitiesBase: LoadableResource<SubactivitiesResource>;
  subactivitiesLoading: boolean;
  projects: LoadableResource<ProjectsResource>;
  projectsBase: LoadableResource<ProjectsResource>;
  projectsLoading: boolean;
  providers: LoadableResource<ProvidersResource>;
  providersBase: LoadableResource<ProvidersResource>;
  providersLoading: boolean;
  positions: LoadableResource<ProcessedPositionsResource>;
  positionsBase: LoadableResource<PositionsResource>;
  positionsLoading: boolean;
  skills: LoadableResource<ProcessedSkillsResource>;
  skillsBase: LoadableResource<SkillsResource>;
  skillsLoading: boolean;
  tags: LoadableResource<TagsResource>;
  tagsBase: LoadableResource<TagsResource>;
  tagsLoading: boolean;
  specializations: LoadableResource<ProcessedSpecializationsResource>;
  specializationsBase: LoadableResource<SpecializationsResource>;
  specializationsLoading: boolean;
}

const useStaticResources = (): UseStaticResources => {
  const {
    version,
    languageBundles,
    cv,
    user,
    activities,
    subactivities,
    projects,
    providers,
    positions,
    skills,
    tags,
    specializations,
  } = useSelector((state: RootState) => state.resourcesReducer);

  return {
    version: version.value,
    versionLoading: version.loading,
    languageBundles: languageBundles.value,
    languageBundlesLoading: languageBundles.loading,
    cv: cv.value,
    cvBase: cv.baseValue,
    cvLoading: cv.loading,
    user: user.value,
    userBase: user.baseValue,
    userLoading: user.loading,
    activities: activities.value,
    activitiesBase: activities.baseValue,
    activitiesLoading: activities.loading,
    subactivities: subactivities.value,
    subactivitiesBase: subactivities.baseValue,
    subactivitiesLoading: subactivities.loading,
    projects: projects.value,
    projectsBase: projects.baseValue,
    projectsLoading: projects.loading,
    providers: providers.value,
    providersBase: providers.baseValue,
    providersLoading: providers.loading,
    positions: positions.value,
    positionsBase: positions.baseValue,
    positionsLoading: positions.loading,
    skills: skills.value,
    skillsBase: skills.baseValue,
    skillsLoading: skills.loading,
    tags: tags.value,
    tagsBase: tags.baseValue,
    tagsLoading: tags.loading,
    specializations: specializations.value,
    specializationsBase: specializations.baseValue,
    specializationsLoading: specializations.loading,
  };
};

export default useStaticResources;
